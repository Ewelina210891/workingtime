﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkingTimeApp
{
    public class PerformedJobItem
    {
        public int PerformedJobId {get; set;}
        public int JobId { get; set; }
        public string ProjectNumber { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool Delegation { get; set; }
        public string Comment { get; set; }
    }
}
