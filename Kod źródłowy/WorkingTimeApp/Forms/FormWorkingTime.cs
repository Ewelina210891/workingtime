﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WorkingTimeApp.Repositories;

namespace WorkingTimeApp
{
    public partial class FormWorkingTime : Form
    {

        #region variables
        public static string projectNumber;
        public static string projectDescription;
        public static DateTime actualDate;
        public static string employeeName;

        private IRepository _repository;
        private bool _projectIsCancel;
        #endregion

        #region constructors
        public FormWorkingTime()
        {
            InitializeComponent();

            SetRepository();
            SetEmployeeName();

            try
            {
                LoadFormWorkingTime();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Nie można nawiązać połączenia z bazą danych!");
                this.Enabled = false;
            }
        }
        #endregion

        #region initial settings
        private void SetRepository()
        {
            _repository = new SqliteRepository(@"WorkingTime.s3db");
            //_repository = new SqlRepository(@"(local)");
        }
       
        private void SetEmployeeName()
        {
            employeeName = "Jan Kowalski";
            lbEmployeeName.Text = employeeName;
        }

        private void LoadFormWorkingTime()
        {
            DisabledEditPerformedJob();
            
            GetNumbersOfProjects();

            CheckThatActualDayIsHoliday();
            CheckThatActualDayIsDayOff();

            CalendarSettings();

            GetPerformedJobs();

            projectNumber = "Wszystkie";
            projectDescription = "Zadania dla wszystkich projektów";

            EnabledRemoveProjectButton();

            WorkTimeInActualDay();
        }

        private void CalendarSettings()
        {
            calendar.MaxDate = DateTime.Now.Date;
            calendar.MaxSelectionCount = 1;
            actualDate = calendar.SelectionStart.Date;
        }
        #endregion

        #region changing settings
        private void FormWorkingTime_Activated(object sender, EventArgs e)
        {
            GetNumbersOfProjects();
            GetPerformedJobs();
            WorkTimeInActualDay();
        }

        private void calendar_DateChanged(object sender, DateRangeEventArgs e)
        {
            actualDate = calendar.SelectionStart.Date;
            uncheckedCb();
            CheckThatActualDayIsHoliday();
            CheckThatActualDayIsDayOff();
            GetPerformedJobs();

            WorkTimeInActualDay();
        }


        private void cboxNumberOfProject_SelectionChangeCommitted(object sender, EventArgs e)
        {
            projectNumber = cboxNumberOfProject.GetItemText(cboxNumberOfProject.SelectedItem);
            projectDescription = cboxNumberOfProject.SelectedValue.ToString();
            lbDescriptionOfProject.Text = cboxNumberOfProject.SelectedValue.ToString();


            CheckThatProjectIsCancel();
            EnabledRemoveProjectButton();
            //EnabledExitProjectButton();

            GetPerformedJobs();
            WorkTimeInActualDay();

            if (_projectIsCancel)
            {
                disabledCbDayOffAndCbHoliday();

            }
            else
                uncheckedCb();
            CheckThatActualDayIsHoliday();
            CheckThatActualDayIsDayOff();
        }
        #endregion

        #region get projects
        private void GetNumbersOfProjects()
        {
            GetDataForNumbersOfProjects();
            SetDisplayProjectNumber();
        }
      
        private void GetDataForNumbersOfProjects()
        {
            var result = _repository.GetProjects();

            var allProjects = new Object[] { "Wszystkie", "Zadania dla wszystkich projektów" };
            result.Rows.Add(allProjects);

            cboxNumberOfProject.DataSource = result;
            cboxNumberOfProject.DisplayMember = "ProjectNumber";
            cboxNumberOfProject.ValueMember = "Description";
        }
       
        private void SetDisplayProjectNumber()
        {
            var item = cboxNumberOfProject.FindString(projectNumber);
            if (item == -1)
            {
                projectNumber = "Wszystkie";
                item = cboxNumberOfProject.FindString(projectNumber);
            }

            cboxNumberOfProject.SelectedItem = cboxNumberOfProject.Items[item];
            lbDescriptionOfProject.Text = cboxNumberOfProject.SelectedValue.ToString();
            if (!_projectIsCancel)
                EnabledEditPerformedJob();
            else
                DisabledEditPerformedJob();

        }
        #endregion

        #region checking method
        private void CheckThatActualDayIsHoliday()
        {
            if (_repository.CheckThatActualDayIsHoliday(calendar.SelectionStart.Date))
                checkedCbHoliday();
        }
        private void CheckThatActualDayIsDayOff()
        {
            if (_repository.CheckThatActualDayIsDayOff(calendar.SelectionStart.Date))
                checkedCbDayOff();
        }
        private void CheckThatProjectIsCancel()
        {
            if (_repository.CheckThatProjectIsCancel(projectNumber) )
            {
                ProjectIsCancel();
                
            }
            else if(projectNumber!="")
            {
                ProjectIsNotCancel();
            }
        }
        #endregion

        #region project settings
        private void EnabledRemoveProjectButton()
        {
            if (projectNumber != "Wszystkie" && projectNumber != "Czas admin. ")
            {
                btnRemoveProject.Enabled = true;
                btnExitProject.Enabled = true;
                btnResumeProject.Enabled = true;
            }
            else
            {
                btnRemoveProject.Enabled = false;
                btnExitProject.Enabled = false;
                btnResumeProject.Enabled = false;
            }
        }

        private void ProjectIsCancel()
        {
            _projectIsCancel = true;

            lbDescriptionOfProject.Text += " ZAKOŃCZONY";

            btnExitProject.Visible = false;
            btnResumeProject.Visible = true;
            DisabledEditPerformedJob();
        }

        private void ProjectIsNotCancel()
        {
            lbDescriptionOfProject.Text = cboxNumberOfProject.SelectedValue.ToString();

            _projectIsCancel = false;
            btnExitProject.Visible = true;
            btnResumeProject.Visible = false;
            EnabledEditPerformedJob();
        }
        #endregion

        #region dataGridView settings

        private void GetPerformedJobs()
        {
            if (projectNumber == "Wszystkie")
            {
                GetPerformedJobsForAllProjects();
            }
            else
            {
                GetPerformedJobsForOneProject();
            }
            SetColumnWidths();
            SetColumnAligments();
            SetColumnNames();
        }

        private void GetPerformedJobsForOneProject()
        {
            var date = calendar.SelectionRange.Start;

            var result = _repository.GetPerformedJobsForOneProjectOnOneDay(date, projectNumber);

            dgvJobs.DataSource = result;
            dgvJobs.Columns["PerformedJobId"].Visible = false;
            dgvJobs.Columns["JobId"].Visible = false;
            dgvJobs.Columns["ProjectNumber"].Visible = false;
        }
        private void GetPerformedJobsForAllProjects()
        {
            var date = calendar.SelectionRange.Start;

            var result = _repository.GetPerformedJobsForAllProjectsOnOneDay(date);

            dgvJobs.DataSource = result;
            dgvJobs.Columns["PerformedJobId"].Visible = false;
            dgvJobs.Columns["JobId"].Visible = false;
            dgvJobs.Columns["ProjectNumber"].Visible = true;
        }

        private void SetColumnWidths()
        {
            dgvJobs.Columns[2].Width = 105;
            dgvJobs.Columns[3].Width = 250;
            dgvJobs.Columns[4].Width = 75;
            dgvJobs.Columns[5].Width = 75;
            dgvJobs.Columns[6].Width = 65;
            dgvJobs.Columns[7].Width = 670;
        }
        private void SetColumnNames()
        {
            dgvJobs.Columns[2].HeaderText = "Nr projektu";
            dgvJobs.Columns[3].HeaderText = "Zadanie";
            dgvJobs.Columns[4].HeaderText = "Rozpoczęto";
            dgvJobs.Columns[5].HeaderText = "Zakończono";
            dgvJobs.Columns[6].HeaderText = "Delegacja";
            dgvJobs.Columns[7].HeaderText = "Uwagi";
        }
        private void SetColumnAligments()
        {
            dgvJobs.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvJobs.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void EnabledEditPerformedJob()
        {
            btnAddPerformedJob.Enabled = true;
            btnRemovePerformedJob.Enabled = true;
            btnEditPerformedJob.Enabled = true;
        }
        private void DisabledEditPerformedJob()
        {
            btnAddPerformedJob.Enabled = false;
            btnRemovePerformedJob.Enabled = false;
            btnEditPerformedJob.Enabled = false;
        }
        #endregion

        #region work time
        private void WorkTimeInActualDay()
        {
            if (projectNumber == "Wszystkie")
            {
                WorkTimeInActualDayForAllProjects();
            }
            else
            {
                WorkTimeInActualDayForOneProject();
            }
        }

        private void WorkTimeInActualDayForAllProjects()
        {
            var workingTimeMinutes = _repository.GetWorkTimeInActualDayForAllProjects(actualDate);
            var delegationTimeMinutes = _repository.GetDelegationTimeInActualDayForAllProjects(actualDate);

            lbTime.Text = ConvertToTimeString(workingTimeMinutes);
            lbDelegation.Text = ConvertToTimeString(delegationTimeMinutes);
        }
        private void WorkTimeInActualDayForOneProject()
        {
            var workingTimeMinutes = _repository.GetWorkTimeInActualDayForOneProject(actualDate, projectNumber);
            var delegationTimeMinutes = _repository.GetDelegationTimeInActualDayForOneProject(actualDate, projectNumber);

            lbTime.Text = ConvertToTimeString(workingTimeMinutes);
            lbDelegation.Text = ConvertToTimeString(delegationTimeMinutes);
        }
        #endregion

        #region buttons

        private void btnAddProject_Click(object sender, EventArgs e)
        {
            var addNewProjectForm = new FormAddNewProject(_repository);
            addNewProjectForm.Show();
        }

        private void btnExitProject_Click(object sender, EventArgs e)
        {           
            _repository.CancelProject(projectNumber);
            ProjectIsCancel();
        }

        private void btnResumeProject_Click(object sender, EventArgs e)
        {
            _repository.ResumeProject(projectNumber);
            ProjectIsNotCancel();
        }

        private void btnRemoveProject_Click(object sender, EventArgs e)
        {
            var dialogResult = MessageBox.Show("Czy na pewno chcesz usunąć projekt? Zostaną usunięte wszystkie wykonane zadania powiązane z tym projektem.", "Potwierdzenie usunięcia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                _repository.RemoveProject(projectNumber);
                _repository.RemovePerformedJobsFromProject(projectNumber);
                GetNumbersOfProjects();
                MessageBox.Show("Usunięto projekt");
            }
        }
        private void btnEditPerformedJob_Click(object sender, EventArgs e)
        {
            if (dgvJobs.SelectedRows.Count == 1)
            {
                DataGridViewCellCollection wiersz = dgvJobs.SelectedRows[0].Cells;

                var performedJobItem = new PerformedJobItem
                {
                    PerformedJobId = Convert.ToInt32(wiersz["PerformedJobId"].Value.ToString()),
                    JobId = Convert.ToInt32(wiersz["JobId"].Value.ToString()),
                    Date = actualDate,
                    ProjectNumber = wiersz["ProjectNumber"].Value.ToString(),
                    Description = wiersz["Description"].Value.ToString(),
                    StartTime = DateTime.ParseExact(wiersz["StartTime"].Value.ToString(), "HH:mm", System.Globalization.CultureInfo.InvariantCulture),
                    EndTime = DateTime.ParseExact(wiersz["EndTime"].Value.ToString(), "HH:mm", System.Globalization.CultureInfo.InvariantCulture),
                    Delegation = Convert.ToBoolean(wiersz["Delegation"].Value),
                    Comment = wiersz["Comment"].Value.ToString()
                };

                var editPerformedJobs = new FormPerformedJob(_repository, performedJobItem);
                editPerformedJobs.Show();
            }
        }

        private void btnAddPerformedJob_Click(object sender, EventArgs e)
        {
            var performedJob = new FormPerformedJob(_repository);
            performedJob.Show();
        }

        private void btnRemovePerformedJob_Click(object sender, EventArgs e)
        {
            if (dgvJobs.SelectedRows.Count == 1)
            {
                DataGridViewCellCollection wiersz = dgvJobs.SelectedRows[0].Cells;

                var performedJobId = Convert.ToInt32(wiersz[0].Value.ToString());
                _repository.RemovePerformedJob(performedJobId);
                GetPerformedJobs();
                WorkTimeInActualDay();
            }
        }

        private void btnEditJobs_Click(object sender, EventArgs e)
        {
            var jobs = new FormJobs(_repository);
            jobs.Show();
        }

        private void btnGenerateReport_Click(object sender, EventArgs e)
        {
            var generateReport = new FormGenerateReport(_repository);
            generateReport.Show();
        }
        #endregion 

        #region checkbox settings
        private void cbHoliday_Click(object sender, EventArgs e)
        {
            if (cbHoliday.Checked)
            {
                _repository.AddHoliday(calendar.SelectionStart.Date);
                checkedCbHoliday();
            }
            else
            {
                _repository.RemoveHoliday(calendar.SelectionStart.Date);
                uncheckedCb();
            }
        }
        private void cbDayOff_Click(object sender, EventArgs e)
        {
            if (cbDayOff.Checked)
            {
                if (dgvJobs.Rows.Count > 0)
                {
                    cbDayOff.Checked = false;
                    MessageBox.Show("Nie można oznaczyć dnia jako wolny - usuń wszystkie zadania z tego dnia!");
                }
                else
                {
                    _repository.AddDayOff(calendar.SelectionStart.Date);
                    checkedCbDayOff();
                }
            }
            else
            {
                _repository.RemoveDayOff(calendar.SelectionStart.Date);
                uncheckedCb();
            }
        }

        private void checkedCbHoliday()
        {
            cbHoliday.Checked = true;
            cbDayOff.Enabled = false;
        }

        private void uncheckedCb()
        {
            cbHoliday.Checked = false;
            cbDayOff.Checked = false;
            cbHoliday.Enabled = true;
            cbDayOff.Enabled = true;


            if (!_projectIsCancel) EnabledEditPerformedJob();
            else EnabledEditPerformedJob();
        }

        private void checkedCbDayOff()
        {
            cbDayOff.Checked = true;
            cbHoliday.Enabled = false;

            DisabledEditPerformedJob();
        }

        private void disabledCbDayOffAndCbHoliday()
        {
            cbHoliday.Checked = false;
            cbDayOff.Checked = false;
            cbHoliday.Enabled = false;
            cbDayOff.Enabled = false;
        }
        #endregion

        private string ConvertToTimeString(int minutes)
        {
            var hour = minutes / 60;
            var minute = minutes % 60;

            if
                (minute < 10) return hour + ":0" + minute;
            else
                return hour + ":" + minute;
        }

    }
}
