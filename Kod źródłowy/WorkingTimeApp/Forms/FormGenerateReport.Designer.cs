﻿namespace WorkingTimeApp
{
    partial class FormGenerateReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbTypeOfReport = new System.Windows.Forms.Label();
            this.btnSaveReport = new System.Windows.Forms.Button();
            this.cbTypeOfReport = new System.Windows.Forms.ComboBox();
            this.mcRangeOfDays = new System.Windows.Forms.MonthCalendar();
            this.label1 = new System.Windows.Forms.Label();
            this.rbReportForProject = new System.Windows.Forms.RadioButton();
            this.rbReportForRange = new System.Windows.Forms.RadioButton();
            this.cbChooseProject = new System.Windows.Forms.ComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // lbTypeOfReport
            // 
            this.lbTypeOfReport.AutoSize = true;
            this.lbTypeOfReport.Location = new System.Drawing.Point(17, 22);
            this.lbTypeOfReport.Name = "lbTypeOfReport";
            this.lbTypeOfReport.Size = new System.Drawing.Size(74, 15);
            this.lbTypeOfReport.TabIndex = 0;
            this.lbTypeOfReport.Text = "Typ raportu:";
            // 
            // btnSaveReport
            // 
            this.btnSaveReport.BackColor = System.Drawing.SystemColors.Menu;
            this.btnSaveReport.Location = new System.Drawing.Point(37, 373);
            this.btnSaveReport.Name = "btnSaveReport";
            this.btnSaveReport.Size = new System.Drawing.Size(180, 30);
            this.btnSaveReport.TabIndex = 7;
            this.btnSaveReport.Text = "Generuj raport do pliku";
            this.toolTip1.SetToolTip(this.btnSaveReport, "Generuj raport do wybranego pliku.");
            this.btnSaveReport.UseVisualStyleBackColor = false;
            this.btnSaveReport.Click += new System.EventHandler(this.btnSaveReport_Click);
            // 
            // cbTypeOfReport
            // 
            this.cbTypeOfReport.FormattingEnabled = true;
            this.cbTypeOfReport.Items.AddRange(new object[] {
            "Ogólny",
            "Dzień po dniu"});
            this.cbTypeOfReport.Location = new System.Drawing.Point(67, 41);
            this.cbTypeOfReport.Name = "cbTypeOfReport";
            this.cbTypeOfReport.Size = new System.Drawing.Size(179, 23);
            this.cbTypeOfReport.TabIndex = 1;
            this.toolTip1.SetToolTip(this.cbTypeOfReport, "Wybierz rodzaj raportu, który chcesz wygenerować.");
            // 
            // mcRangeOfDays
            // 
            this.mcRangeOfDays.Location = new System.Drawing.Point(67, 188);
            this.mcRangeOfDays.Margin = new System.Windows.Forms.Padding(10);
            this.mcRangeOfDays.MaxSelectionCount = 366;
            this.mcRangeOfDays.Name = "mcRangeOfDays";
            this.mcRangeOfDays.TabIndex = 6;
            this.toolTip1.SetToolTip(this.mcRangeOfDays, "Wybierz zakres dni, z którego chcesz wygenerować raport.");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Zakres raportu:";
            // 
            // rbReportForProject
            // 
            this.rbReportForProject.AutoSize = true;
            this.rbReportForProject.Location = new System.Drawing.Point(34, 102);
            this.rbReportForProject.Name = "rbReportForProject";
            this.rbReportForProject.Size = new System.Drawing.Size(99, 19);
            this.rbReportForProject.TabIndex = 3;
            this.rbReportForProject.TabStop = true;
            this.rbReportForProject.Text = "jeden projekt";
            this.rbReportForProject.UseVisualStyleBackColor = true;
            this.rbReportForProject.CheckedChanged += new System.EventHandler(this.rbReportForProject_CheckedChanged);
            // 
            // rbReportForRange
            // 
            this.rbReportForRange.AutoSize = true;
            this.rbReportForRange.Location = new System.Drawing.Point(34, 155);
            this.rbReportForRange.Name = "rbReportForRange";
            this.rbReportForRange.Size = new System.Drawing.Size(189, 19);
            this.rbReportForRange.TabIndex = 5;
            this.rbReportForRange.TabStop = true;
            this.rbReportForRange.Text = "wszystkie projekty z zakresu";
            this.rbReportForRange.UseVisualStyleBackColor = true;
            // 
            // cbChooseProject
            // 
            this.cbChooseProject.FormattingEnabled = true;
            this.cbChooseProject.Items.AddRange(new object[] {
            "Ogólny",
            "Dzień po dniu"});
            this.cbChooseProject.Location = new System.Drawing.Point(67, 128);
            this.cbChooseProject.Name = "cbChooseProject";
            this.cbChooseProject.Size = new System.Drawing.Size(179, 23);
            this.cbChooseProject.TabIndex = 4;
            this.toolTip1.SetToolTip(this.cbChooseProject, "Wybierz numer projektu z listy.");
            // 
            // FormGenerateReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(271, 417);
            this.Controls.Add(this.cbChooseProject);
            this.Controls.Add(this.rbReportForRange);
            this.Controls.Add(this.rbReportForProject);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.mcRangeOfDays);
            this.Controls.Add(this.cbTypeOfReport);
            this.Controls.Add(this.btnSaveReport);
            this.Controls.Add(this.lbTypeOfReport);
            this.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.Name = "FormGenerateReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "GenerateReportForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbTypeOfReport;
        private System.Windows.Forms.Button btnSaveReport;
        private System.Windows.Forms.ComboBox cbTypeOfReport;
        private System.Windows.Forms.MonthCalendar mcRangeOfDays;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbReportForProject;
        private System.Windows.Forms.RadioButton rbReportForRange;
        private System.Windows.Forms.ComboBox cbChooseProject;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}