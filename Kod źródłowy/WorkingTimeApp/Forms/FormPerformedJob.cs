﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WorkingTimeApp.Repositories;

namespace WorkingTimeApp
{

    public partial class FormPerformedJob : Form
    {

        #region variables
        private IRepository _repository;
        private PerformedJobItem _performedJob;
        #endregion

        #region constructors
        public FormPerformedJob(IRepository repository)
        {
            _repository = repository;
            InitializeComponent();
            SetFormatOfTime();

            btnAddNewPerformedJob.Visible = true;
            btnUpdatePerformedJob.Visible = false;

            GetNumbersOfProjectForAddNewPerformedJob();


            lbDescriptionOfProject.Text = cbChooseProjectNumber.SelectedValue.ToString();

        }
        public FormPerformedJob(IRepository repository, PerformedJobItem performedJobItem)
        {
            _repository = repository;

            InitializeComponent();

            GetNumbersOfProject();

            var item = cbChooseProjectNumber.FindString(performedJobItem.ProjectNumber.Trim());
            cbChooseProjectNumber.SelectedItem = cbChooseProjectNumber.Items[item];
            lbDescriptionOfProject.Text = cbChooseProjectNumber.SelectedValue.ToString();

            GetJob(performedJobItem.JobId);

            btnAddNewPerformedJob.Visible = false;
            btnUpdatePerformedJob.Visible = true;

            this._performedJob = performedJobItem;

            LoadFormPerformedJob();
        }
        #endregion

        #region initial settings
        private void LoadFormPerformedJob()
        {
            dtpStartTime.Value = _performedJob.StartTime;
            dtpEndTime.Value = _performedJob.EndTime;
            rtxtComments.Text = _performedJob.Comment;
            cbDelegation.Checked = _performedJob.Delegation;

            SetFormatOfTime();
        }
        private void SetFormatOfTime()
        {
            dtpStartTime.Format = DateTimePickerFormat.Custom;
            dtpStartTime.CustomFormat = "HH:mm";
            dtpStartTime.ShowUpDown = true;

            dtpEndTime.Format = DateTimePickerFormat.Custom;
            dtpEndTime.CustomFormat = "HH:mm";
            dtpEndTime.ShowUpDown = true;
        }
        #endregion

        #region get projects
        private void GetNumbersOfProjectForAddNewPerformedJob()
        {
            if (FormWorkingTime.projectNumber == "Wszystkie")
                GetNumbersOfProject();
            else
            {
                var projects = new ProjectItem { ProjectNumber = FormWorkingTime.projectNumber, ProjectDescription = FormWorkingTime.projectDescription };
                cbChooseProjectNumber.Items.Add(projects);
                cbChooseProjectNumber.DisplayMember = "ProjectNumber";
                cbChooseProjectNumber.ValueMember = FormWorkingTime.projectDescription;
                cbChooseProjectNumber.SelectedItem = cbChooseProjectNumber.Items[0];
            }
        }

        private void GetNumbersOfProject()
        {
            var result = _repository.GetProjects();            
            cbChooseProjectNumber.DataSource = result;
            cbChooseProjectNumber.DisplayMember = "ProjectNumber";
            cbChooseProjectNumber.ValueMember = "Description";
            
        }
        #endregion
 
        #region changing settings
        private void FormPerformedJob_Activated(object sender, EventArgs e)
        {             
                GetJobs();
        }
        private void cbChooseProjectNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbChooseProjectNumber.SelectedValue != null && lbDescriptionOfProject != null) lbDescriptionOfProject.Text = cbChooseProjectNumber.SelectedValue.ToString();
        }
        #endregion

        #region jobs
        private void GetJobs()
        {
            var result = _repository.GetAllJobs();
            cbChooseJob.DataSource = result;
            cbChooseJob.DisplayMember = "Description";
            cbChooseJob.ValueMember = "JobId";
        }
        private void GetJob(int jobId)
        {
            var result = _repository.GetJob(jobId);
            cbChooseJob.DataSource = result;
            cbChooseJob.DisplayMember = "Description";
            cbChooseJob.ValueMember = "JobId";
        }
        #endregion

        #region time settings
        private void dtpStartTime_Leave(object sender, EventArgs e)
        {
            if (dtpEndTime.Value <= dtpStartTime.Value)
            {               
                    dtpEndTime.Value = dtpStartTime.Value;
                    dtpEndTime.Value = dtpEndTime.Value.AddMinutes(1.0);
            }
        }
        private void dtpEndTime_Leave(object sender, EventArgs e)
        {
            if (dtpEndTime.Value <= dtpStartTime.Value)
            {
                dtpStartTime.Value = dtpEndTime.Value;
                dtpStartTime.Value=dtpStartTime.Value.AddMinutes(-1.0);
            }
                
        }

        private bool CheckTimeOfPerformedJobInOneDay(DateTime date, DateTime startTime, DateTime endTime, int performedJobId)
        {
            var result = _repository.GetTimeOfPerformedJobsInOneDay(date, performedJobId);

            foreach (DataRow dtRow in result.Rows)
            {
                var timeStart = DateTime.ParseExact(dtRow[0].ToString(), "HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).TimeOfDay;
                var timeEnd = DateTime.ParseExact(dtRow[1].ToString(), "HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).TimeOfDay;

                if (startTime.TimeOfDay < timeStart && endTime.TimeOfDay > timeStart)
                {
                    return false;
                }
                if (startTime.TimeOfDay > endTime.TimeOfDay)
                {
                    return false;
                }
                if (startTime.TimeOfDay == endTime.TimeOfDay)
                {
                    return false;
                }
                if (startTime.TimeOfDay < timeEnd && startTime.TimeOfDay >= timeStart)
                {
                    return false;
                }
            }
            return true;
        }
        #endregion   

        #region buttons
        private void btnAddNewPerformedJob_Click(object sender, EventArgs e)
        {

            if (CheckTimeOfPerformedJobInOneDay(FormWorkingTime.actualDate, dtpStartTime.Value, dtpEndTime.Value, 0))
            {
                _performedJob = new PerformedJobItem
                {
                    JobId = Convert.ToInt32(cbChooseJob.SelectedValue.ToString()),
                    ProjectNumber = cbChooseProjectNumber.GetItemText(cbChooseProjectNumber.SelectedItem),
                    Date = FormWorkingTime.actualDate,
                    StartTime = dtpStartTime.Value,
                    EndTime = dtpEndTime.Value,
                    Delegation = cbDelegation.Checked,
                    Comment = rtxtComments.Text
                };

                var projectNumber = FormWorkingTime.projectNumber;
                var date = FormWorkingTime.actualDate;

                _repository.AddPerformedJob(_performedJob);
                this.Close();
            }
            else
            {
                MessageBox.Show("Nie dodano. Czas wykonania nowego zadania nie może się pokrywać z czasem innych wykonanych zadań w tym dniu", "Błąd podczas dodawania wykonanego zadania", MessageBoxButtons.OK);
            }
        }
        private void btnUpdatePerformedJob_Click(object sender, EventArgs e)
        {
            if (CheckTimeOfPerformedJobInOneDay(FormWorkingTime.actualDate, dtpStartTime.Value, dtpEndTime.Value, _performedJob.PerformedJobId))
            {
                _performedJob.ProjectNumber = cbChooseProjectNumber.GetItemText(cbChooseProjectNumber.SelectedItem);
                _performedJob.JobId = Convert.ToInt32(cbChooseJob.SelectedValue.ToString());
                _performedJob.StartTime = dtpStartTime.Value;
                _performedJob.EndTime = dtpEndTime.Value;
                _performedJob.Delegation = cbDelegation.Checked;
                _performedJob.Comment = rtxtComments.Text;
                _repository.UpdatePerformedJob(_performedJob);
                this.Close();
            }
            else
            {
                MessageBox.Show("Czas wykonania nowego zadania nie może się pokrywać z czasem innych wykonanych zadań w tym dniu", "Błąd podczas edycji wykonanego zadania", MessageBoxButtons.OK);
            }
        }

        private void btnAddNewJob_Click(object sender, EventArgs e)
        {
            var addJob = new FormAddNewJob(_repository);
            addJob.Show();
        }

        private void btnAddNewProject_Click(object sender, EventArgs e)
        {
            var addNewProjectForm = new FormAddNewProject(_repository);
            addNewProjectForm.Show();
        }
  
        private void btnCancelAddPerformedJob_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
