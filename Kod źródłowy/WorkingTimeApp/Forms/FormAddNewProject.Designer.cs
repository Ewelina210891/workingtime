﻿namespace WorkingTimeApp
{
    partial class FormAddNewProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbProjectName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtProjectName = new System.Windows.Forms.TextBox();
            this.btnAddNewProject = new System.Windows.Forms.Button();
            this.rtxtDescriptionOfProject = new System.Windows.Forms.RichTextBox();
            this.btnCancelAddProject = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // lbProjectName
            // 
            this.lbProjectName.AutoSize = true;
            this.lbProjectName.Location = new System.Drawing.Point(20, 20);
            this.lbProjectName.Name = "lbProjectName";
            this.lbProjectName.Size = new System.Drawing.Size(98, 15);
            this.lbProjectName.TabIndex = 0;
            this.lbProjectName.Text = "Numer projektu:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Opis projektu:";
            // 
            // txtProjectName
            // 
            this.txtProjectName.Location = new System.Drawing.Point(22, 39);
            this.txtProjectName.MaxLength = 12;
            this.txtProjectName.Name = "txtProjectName";
            this.txtProjectName.Size = new System.Drawing.Size(209, 21);
            this.txtProjectName.TabIndex = 1;
            this.toolTip1.SetToolTip(this.txtProjectName, "Numer musi składać się z dokładnie 11 znaków!");
            // 
            // btnAddNewProject
            // 
            this.btnAddNewProject.BackColor = System.Drawing.SystemColors.Menu;
            this.btnAddNewProject.Location = new System.Drawing.Point(45, 182);
            this.btnAddNewProject.Name = "btnAddNewProject";
            this.btnAddNewProject.Size = new System.Drawing.Size(87, 27);
            this.btnAddNewProject.TabIndex = 3;
            this.btnAddNewProject.Text = "Dodaj";
            this.toolTip1.SetToolTip(this.btnAddNewProject, "Dodanie nowego projektu");
            this.btnAddNewProject.UseVisualStyleBackColor = false;
            this.btnAddNewProject.Click += new System.EventHandler(this.btnAddNewProject_Click);
            // 
            // rtxtDescriptionOfProject
            // 
            this.rtxtDescriptionOfProject.Location = new System.Drawing.Point(22, 96);
            this.rtxtDescriptionOfProject.MaxLength = 50;
            this.rtxtDescriptionOfProject.Name = "rtxtDescriptionOfProject";
            this.rtxtDescriptionOfProject.Size = new System.Drawing.Size(269, 67);
            this.rtxtDescriptionOfProject.TabIndex = 3;
            this.rtxtDescriptionOfProject.Text = "";
            this.toolTip1.SetToolTip(this.rtxtDescriptionOfProject, "Opis projektu nie może być dłuższy niż 50 znaków.");
            this.rtxtDescriptionOfProject.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rtxtDescriptionOfProject_KeyDown);
            // 
            // btnCancelAddProject
            // 
            this.btnCancelAddProject.BackColor = System.Drawing.SystemColors.Menu;
            this.btnCancelAddProject.Location = new System.Drawing.Point(152, 182);
            this.btnCancelAddProject.Name = "btnCancelAddProject";
            this.btnCancelAddProject.Size = new System.Drawing.Size(87, 27);
            this.btnCancelAddProject.TabIndex = 4;
            this.btnCancelAddProject.Text = "Anuluj";
            this.toolTip1.SetToolTip(this.btnCancelAddProject, "Powrót do wcześniejszego okna.");
            this.btnCancelAddProject.UseVisualStyleBackColor = false;
            this.btnCancelAddProject.Click += new System.EventHandler(this.btnCancelAddProject_Click);
            // 
            // FormAddNewProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(313, 224);
            this.Controls.Add(this.btnCancelAddProject);
            this.Controls.Add(this.rtxtDescriptionOfProject);
            this.Controls.Add(this.btnAddNewProject);
            this.Controls.Add(this.txtProjectName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbProjectName);
            this.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.Name = "FormAddNewProject";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Nowy projekt";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbProjectName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtProjectName;
        private System.Windows.Forms.RichTextBox rtxtDescriptionOfProject;
        private System.Windows.Forms.Button btnAddNewProject;
        private System.Windows.Forms.Button btnCancelAddProject;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}