﻿namespace WorkingTimeApp
{
    partial class FormPerformedJob
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbJob = new System.Windows.Forms.Label();
            this.lbTime = new System.Windows.Forms.Label();
            this.lbComment = new System.Windows.Forms.Label();
            this.btnAddNewPerformedJob = new System.Windows.Forms.Button();
            this.btnCancelAddPerformedJob = new System.Windows.Forms.Button();
            this.cbChooseJob = new System.Windows.Forms.ComboBox();
            this.rtxtComments = new System.Windows.Forms.RichTextBox();
            this.btnAddNewJob = new System.Windows.Forms.Button();
            this.dtpStartTime = new System.Windows.Forms.DateTimePicker();
            this.cbChooseProjectNumber = new System.Windows.Forms.ComboBox();
            this.lbProjectNumber = new System.Windows.Forms.Label();
            this.btnAddNewProject = new System.Windows.Forms.Button();
            this.lbDescriptionOfProject = new System.Windows.Forms.Label();
            this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbDelegation = new System.Windows.Forms.CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnUpdatePerformedJob = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbJob
            // 
            this.lbJob.AutoSize = true;
            this.lbJob.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbJob.Location = new System.Drawing.Point(15, 89);
            this.lbJob.Name = "lbJob";
            this.lbJob.Size = new System.Drawing.Size(55, 15);
            this.lbJob.TabIndex = 4;
            this.lbJob.Text = "Zadanie:";
            // 
            // lbTime
            // 
            this.lbTime.AutoSize = true;
            this.lbTime.Location = new System.Drawing.Point(15, 119);
            this.lbTime.Name = "lbTime";
            this.lbTime.Size = new System.Drawing.Size(38, 15);
            this.lbTime.TabIndex = 7;
            this.lbTime.Text = "Czas:";
            // 
            // lbComment
            // 
            this.lbComment.AutoSize = true;
            this.lbComment.Location = new System.Drawing.Point(15, 175);
            this.lbComment.Name = "lbComment";
            this.lbComment.Size = new System.Drawing.Size(45, 15);
            this.lbComment.TabIndex = 13;
            this.lbComment.Text = "Uwagi:";
            // 
            // btnAddNewPerformedJob
            // 
            this.btnAddNewPerformedJob.BackColor = System.Drawing.SystemColors.Menu;
            this.btnAddNewPerformedJob.Location = new System.Drawing.Point(274, 236);
            this.btnAddNewPerformedJob.Name = "btnAddNewPerformedJob";
            this.btnAddNewPerformedJob.Size = new System.Drawing.Size(87, 27);
            this.btnAddNewPerformedJob.TabIndex = 15;
            this.btnAddNewPerformedJob.Text = "Dodaj";
            this.toolTip1.SetToolTip(this.btnAddNewPerformedJob, "Dodaj wykonane zadanie.");
            this.btnAddNewPerformedJob.UseVisualStyleBackColor = false;
            this.btnAddNewPerformedJob.Click += new System.EventHandler(this.btnAddNewPerformedJob_Click);
            // 
            // btnCancelAddPerformedJob
            // 
            this.btnCancelAddPerformedJob.BackColor = System.Drawing.SystemColors.Menu;
            this.btnCancelAddPerformedJob.Location = new System.Drawing.Point(372, 235);
            this.btnCancelAddPerformedJob.Name = "btnCancelAddPerformedJob";
            this.btnCancelAddPerformedJob.Size = new System.Drawing.Size(87, 30);
            this.btnCancelAddPerformedJob.TabIndex = 16;
            this.btnCancelAddPerformedJob.Text = "Anuluj";
            this.toolTip1.SetToolTip(this.btnCancelAddPerformedJob, "Powrót do wcześniejszego okna.");
            this.btnCancelAddPerformedJob.UseVisualStyleBackColor = false;
            this.btnCancelAddPerformedJob.Click += new System.EventHandler(this.btnCancelAddPerformedJob_Click);
            // 
            // cbChooseJob
            // 
            this.cbChooseJob.FormattingEnabled = true;
            this.cbChooseJob.Location = new System.Drawing.Point(90, 84);
            this.cbChooseJob.Name = "cbChooseJob";
            this.cbChooseJob.Size = new System.Drawing.Size(542, 23);
            this.cbChooseJob.TabIndex = 5;
            this.toolTip1.SetToolTip(this.cbChooseJob, "Wybierz zadanie z listy.");
            // 
            // rtxtComments
            // 
            this.rtxtComments.Location = new System.Drawing.Point(90, 172);
            this.rtxtComments.MaxLength = 70;
            this.rtxtComments.Name = "rtxtComments";
            this.rtxtComments.Size = new System.Drawing.Size(542, 48);
            this.rtxtComments.TabIndex = 14;
            this.rtxtComments.Text = "";
            this.toolTip1.SetToolTip(this.rtxtComments, "Wpisz dodatkowe uwagi (pole nie jest wymagane).");
            // 
            // btnAddNewJob
            // 
            this.btnAddNewJob.BackColor = System.Drawing.SystemColors.Menu;
            this.btnAddNewJob.Location = new System.Drawing.Point(647, 80);
            this.btnAddNewJob.Name = "btnAddNewJob";
            this.btnAddNewJob.Size = new System.Drawing.Size(101, 30);
            this.btnAddNewJob.TabIndex = 6;
            this.btnAddNewJob.Text = "Dodaj nowe";
            this.toolTip1.SetToolTip(this.btnAddNewJob, "Dodaj nowe zadanie.");
            this.btnAddNewJob.UseVisualStyleBackColor = false;
            this.btnAddNewJob.Click += new System.EventHandler(this.btnAddNewJob_Click);
            // 
            // dtpStartTime
            // 
            this.dtpStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartTime.Location = new System.Drawing.Point(119, 115);
            this.dtpStartTime.Name = "dtpStartTime";
            this.dtpStartTime.ShowUpDown = true;
            this.dtpStartTime.Size = new System.Drawing.Size(95, 21);
            this.dtpStartTime.TabIndex = 9;
            this.toolTip1.SetToolTip(this.dtpStartTime, "Wpisz godzinę rozpoczęcia zadania.");
            this.dtpStartTime.Value = new System.DateTime(2014, 6, 30, 7, 0, 0, 0);
            this.dtpStartTime.Leave += new System.EventHandler(this.dtpStartTime_Leave);
            // 
            // cbChooseProjectNumber
            // 
            this.cbChooseProjectNumber.FormattingEnabled = true;
            this.cbChooseProjectNumber.Location = new System.Drawing.Point(90, 24);
            this.cbChooseProjectNumber.Name = "cbChooseProjectNumber";
            this.cbChooseProjectNumber.Size = new System.Drawing.Size(221, 23);
            this.cbChooseProjectNumber.TabIndex = 1;
            this.toolTip1.SetToolTip(this.cbChooseProjectNumber, "Wybierz numer projektu z listy.");
            this.cbChooseProjectNumber.SelectedIndexChanged += new System.EventHandler(this.cbChooseProjectNumber_SelectedIndexChanged);
            // 
            // lbProjectNumber
            // 
            this.lbProjectNumber.AutoSize = true;
            this.lbProjectNumber.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbProjectNumber.Location = new System.Drawing.Point(15, 32);
            this.lbProjectNumber.Name = "lbProjectNumber";
            this.lbProjectNumber.Size = new System.Drawing.Size(73, 15);
            this.lbProjectNumber.TabIndex = 0;
            this.lbProjectNumber.Text = "Nr projektu:";
            // 
            // btnAddNewProject
            // 
            this.btnAddNewProject.BackColor = System.Drawing.SystemColors.Menu;
            this.btnAddNewProject.Location = new System.Drawing.Point(329, 20);
            this.btnAddNewProject.Name = "btnAddNewProject";
            this.btnAddNewProject.Size = new System.Drawing.Size(101, 30);
            this.btnAddNewProject.TabIndex = 2;
            this.btnAddNewProject.Text = "Dodaj nowy";
            this.toolTip1.SetToolTip(this.btnAddNewProject, "Dodaj nowy projekt.");
            this.btnAddNewProject.UseVisualStyleBackColor = false;
            this.btnAddNewProject.Click += new System.EventHandler(this.btnAddNewProject_Click);
            // 
            // lbDescriptionOfProject
            // 
            this.lbDescriptionOfProject.AutoSize = true;
            this.lbDescriptionOfProject.Location = new System.Drawing.Point(94, 58);
            this.lbDescriptionOfProject.Name = "lbDescriptionOfProject";
            this.lbDescriptionOfProject.Size = new System.Drawing.Size(0, 15);
            this.lbDescriptionOfProject.TabIndex = 3;
            // 
            // dtpEndTime
            // 
            this.dtpEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndTime.Location = new System.Drawing.Point(255, 115);
            this.dtpEndTime.Name = "dtpEndTime";
            this.dtpEndTime.ShowUpDown = true;
            this.dtpEndTime.Size = new System.Drawing.Size(95, 21);
            this.dtpEndTime.TabIndex = 11;
            this.toolTip1.SetToolTip(this.dtpEndTime, "Wpisz godzinę zakończenia zadania.");
            this.dtpEndTime.Value = new System.DateTime(2014, 6, 30, 7, 1, 0, 0);
            this.dtpEndTime.Leave += new System.EventHandler(this.dtpEndTime_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(90, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 15);
            this.label1.TabIndex = 8;
            this.label1.Text = "od";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(223, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 15);
            this.label2.TabIndex = 10;
            this.label2.Text = "do";
            // 
            // cbDelegation
            // 
            this.cbDelegation.AutoSize = true;
            this.cbDelegation.Location = new System.Drawing.Point(90, 146);
            this.cbDelegation.Name = "cbDelegation";
            this.cbDelegation.Size = new System.Drawing.Size(82, 19);
            this.cbDelegation.TabIndex = 12;
            this.cbDelegation.Text = "Delegacja";
            this.cbDelegation.UseVisualStyleBackColor = true;
            // 
            // btnUpdatePerformedJob
            // 
            this.btnUpdatePerformedJob.BackColor = System.Drawing.SystemColors.Menu;
            this.btnUpdatePerformedJob.Location = new System.Drawing.Point(274, 235);
            this.btnUpdatePerformedJob.Name = "btnUpdatePerformedJob";
            this.btnUpdatePerformedJob.Size = new System.Drawing.Size(87, 30);
            this.btnUpdatePerformedJob.TabIndex = 17;
            this.btnUpdatePerformedJob.Text = "Zapisz";
            this.toolTip1.SetToolTip(this.btnUpdatePerformedJob, "Dodaj wykonane zadanie.");
            this.btnUpdatePerformedJob.UseVisualStyleBackColor = false;
            this.btnUpdatePerformedJob.Click += new System.EventHandler(this.btnUpdatePerformedJob_Click);
            // 
            // FormPerformedJob
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(760, 273);
            this.Controls.Add(this.btnUpdatePerformedJob);
            this.Controls.Add(this.cbDelegation);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpEndTime);
            this.Controls.Add(this.lbDescriptionOfProject);
            this.Controls.Add(this.btnAddNewProject);
            this.Controls.Add(this.cbChooseProjectNumber);
            this.Controls.Add(this.lbProjectNumber);
            this.Controls.Add(this.dtpStartTime);
            this.Controls.Add(this.btnAddNewJob);
            this.Controls.Add(this.rtxtComments);
            this.Controls.Add(this.cbChooseJob);
            this.Controls.Add(this.btnCancelAddPerformedJob);
            this.Controls.Add(this.btnAddNewPerformedJob);
            this.Controls.Add(this.lbComment);
            this.Controls.Add(this.lbTime);
            this.Controls.Add(this.lbJob);
            this.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.Name = "FormPerformedJob";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Dodanie nowego wykonanego zadania";
            this.Activated += new System.EventHandler(this.FormPerformedJob_Activated);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbJob;
        private System.Windows.Forms.Label lbTime;
        private System.Windows.Forms.Label lbComment;
        private System.Windows.Forms.Button btnAddNewPerformedJob;
        private System.Windows.Forms.Button btnCancelAddPerformedJob;
        private System.Windows.Forms.ComboBox cbChooseJob;
        private System.Windows.Forms.RichTextBox rtxtComments;
        private System.Windows.Forms.Button btnAddNewJob;
        private System.Windows.Forms.DateTimePicker dtpStartTime;
        private System.Windows.Forms.ComboBox cbChooseProjectNumber;
        private System.Windows.Forms.Label lbProjectNumber;
        private System.Windows.Forms.Button btnAddNewProject;
        private System.Windows.Forms.Label lbDescriptionOfProject;
        private System.Windows.Forms.DateTimePicker dtpEndTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox cbDelegation;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnUpdatePerformedJob;
    }
}