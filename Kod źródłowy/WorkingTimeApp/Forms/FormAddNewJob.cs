﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WorkingTimeApp.Repositories;

namespace WorkingTimeApp
{
    public partial class FormAddNewJob : Form
    {
        private IRepository _repository;
        private int _jobId;

        public FormAddNewJob(IRepository repository)
        {
            InitializeComponent();

            _repository = repository;

            btnAddNewJob.Visible = true;
            btnUpdateJob.Visible = false;
        }
        public FormAddNewJob(IRepository repository, int jobId, string jobDescription)
        {

            InitializeComponent();
            _repository = repository;
            txtJobName.Text = jobDescription;
            _jobId = jobId;

            btnAddNewJob.Visible = false;
            btnUpdateJob.Visible = true;
        }


        private void btnAddNewJob_Click(object sender, EventArgs e)
        {
          
            
                if (txtJobName.Text == "")
                {
                    MessageBox.Show("Nie można dodać nowego zadania- pole nazwy nie może być puste");
                }
                else
                {
                    _repository.AddJob(txtJobName.Text);
                    MessageBox.Show("Dodano nowe zadanie");
                    this.Close();
                }
            
        }

        private void btnUpdateJob_Click(object sender, EventArgs e)
        {
            if (txtJobName.Text == "")
            {
                MessageBox.Show("Nie można edytować zadania- pole nazwy nie może być puste");
            }
            else
            {
                _repository.UpdateJob(_jobId, txtJobName.Text);
                MessageBox.Show("Zmieniono nazwę zadania");
                this.Close();
            }
        }

        private void btnCancelAddNewJob_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
