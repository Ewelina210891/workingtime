﻿namespace WorkingTimeApp
{
    partial class FormJobs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnAddJob = new System.Windows.Forms.Button();
            this.btnCancelSavingJobs = new System.Windows.Forms.Button();
            this.jobsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.dgvAllJobs = new System.Windows.Forms.DataGridView();
            this.btnDeleteJob = new System.Windows.Forms.Button();
            this.btnEditJob = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.jobsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllJobs)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddJob
            // 
            this.btnAddJob.Location = new System.Drawing.Point(22, 227);
            this.btnAddJob.Name = "btnAddJob";
            this.btnAddJob.Size = new System.Drawing.Size(130, 30);
            this.btnAddJob.TabIndex = 2;
            this.btnAddJob.Text = "Dodaj nowe zadanie";
            this.toolTip1.SetToolTip(this.btnAddJob, "Dodaj nowe zadanie.");
            this.btnAddJob.UseVisualStyleBackColor = true;
            this.btnAddJob.Click += new System.EventHandler(this.btnAddJob_Click);
            // 
            // btnCancelSavingJobs
            // 
            this.btnCancelSavingJobs.Location = new System.Drawing.Point(547, 227);
            this.btnCancelSavingJobs.Name = "btnCancelSavingJobs";
            this.btnCancelSavingJobs.Size = new System.Drawing.Size(100, 30);
            this.btnCancelSavingJobs.TabIndex = 5;
            this.btnCancelSavingJobs.Text = "Wróć";
            this.toolTip1.SetToolTip(this.btnCancelSavingJobs, "Powrót do wcześniejszego okna.");
            this.btnCancelSavingJobs.UseVisualStyleBackColor = true;
            this.btnCancelSavingJobs.Click += new System.EventHandler(this.btnCancelSavingJobs_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Dostępne zadania:";
            // 
            // dgvAllJobs
            // 
            this.dgvAllJobs.AllowUserToAddRows = false;
            this.dgvAllJobs.AllowUserToDeleteRows = false;
            this.dgvAllJobs.BackgroundColor = System.Drawing.SystemColors.Menu;
            this.dgvAllJobs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAllJobs.Location = new System.Drawing.Point(14, 40);
            this.dgvAllJobs.MultiSelect = false;
            this.dgvAllJobs.Name = "dgvAllJobs";
            this.dgvAllJobs.ReadOnly = true;
            this.dgvAllJobs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAllJobs.Size = new System.Drawing.Size(632, 180);
            this.dgvAllJobs.TabIndex = 1;
            // 
            // btnDeleteJob
            // 
            this.btnDeleteJob.Location = new System.Drawing.Point(161, 227);
            this.btnDeleteJob.Name = "btnDeleteJob";
            this.btnDeleteJob.Size = new System.Drawing.Size(100, 30);
            this.btnDeleteJob.TabIndex = 3;
            this.btnDeleteJob.Text = "Usuń";
            this.toolTip1.SetToolTip(this.btnDeleteJob, "Usuń zaznaczone zadanie.");
            this.btnDeleteJob.UseVisualStyleBackColor = true;
            this.btnDeleteJob.Click += new System.EventHandler(this.btnDeleteJob_Click);
            // 
            // btnEditJob
            // 
            this.btnEditJob.Location = new System.Drawing.Point(272, 227);
            this.btnEditJob.Name = "btnEditJob";
            this.btnEditJob.Size = new System.Drawing.Size(100, 30);
            this.btnEditJob.TabIndex = 4;
            this.btnEditJob.Text = "Edytuj";
            this.toolTip1.SetToolTip(this.btnEditJob, "Edytuj nazwę zaznaczonego zadania.");
            this.btnEditJob.UseVisualStyleBackColor = true;
            this.btnEditJob.Click += new System.EventHandler(this.btnEditJob_Click);
            // 
            // FormJobs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(669, 269);
            this.Controls.Add(this.btnEditJob);
            this.Controls.Add(this.btnDeleteJob);
            this.Controls.Add(this.dgvAllJobs);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancelSavingJobs);
            this.Controls.Add(this.btnAddJob);
            this.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.Name = "FormJobs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edycja zadań";
            this.Activated += new System.EventHandler(this.FormJobs_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.jobsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllJobs)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddJob;
        private System.Windows.Forms.Button btnCancelSavingJobs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource jobsBindingSource;
        private System.Windows.Forms.DataGridView dgvAllJobs;
        private System.Windows.Forms.Button btnDeleteJob;
        private System.Windows.Forms.Button btnEditJob;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}