﻿namespace WorkingTimeApp
{
    partial class FormAddNewJob
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbNameOfJob = new System.Windows.Forms.Label();
            this.txtJobName = new System.Windows.Forms.TextBox();
            this.btnAddNewJob = new System.Windows.Forms.Button();
            this.btnCancelAddNewJob = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnUpdateJob = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbNameOfJob
            // 
            this.lbNameOfJob.AutoSize = true;
            this.lbNameOfJob.Location = new System.Drawing.Point(17, 16);
            this.lbNameOfJob.Name = "lbNameOfJob";
            this.lbNameOfJob.Size = new System.Drawing.Size(80, 15);
            this.lbNameOfJob.TabIndex = 0;
            this.lbNameOfJob.Text = "Opis zadania";
            // 
            // txtJobName
            // 
            this.txtJobName.Location = new System.Drawing.Point(19, 40);
            this.txtJobName.MaxLength = 50;
            this.txtJobName.Multiline = true;
            this.txtJobName.Name = "txtJobName";
            this.txtJobName.Size = new System.Drawing.Size(312, 56);
            this.txtJobName.TabIndex = 0;
            this.toolTip1.SetToolTip(this.txtJobName, "Wpisz nazwę (opis) zadania.");
            // 
            // btnAddNewJob
            // 
            this.btnAddNewJob.Location = new System.Drawing.Point(74, 105);
            this.btnAddNewJob.Name = "btnAddNewJob";
            this.btnAddNewJob.Size = new System.Drawing.Size(83, 30);
            this.btnAddNewJob.TabIndex = 1;
            this.btnAddNewJob.Text = "Dodaj";
            this.toolTip1.SetToolTip(this.btnAddNewJob, "Dodanie nowego zadania.");
            this.btnAddNewJob.UseVisualStyleBackColor = true;
            this.btnAddNewJob.Click += new System.EventHandler(this.btnAddNewJob_Click);
            // 
            // btnCancelAddNewJob
            // 
            this.btnCancelAddNewJob.BackColor = System.Drawing.SystemColors.Menu;
            this.btnCancelAddNewJob.Location = new System.Drawing.Point(173, 105);
            this.btnCancelAddNewJob.Name = "btnCancelAddNewJob";
            this.btnCancelAddNewJob.Size = new System.Drawing.Size(83, 30);
            this.btnCancelAddNewJob.TabIndex = 2;
            this.btnCancelAddNewJob.Text = "Anuluj";
            this.toolTip1.SetToolTip(this.btnCancelAddNewJob, "Powrót do wcześniejszego okna.");
            this.btnCancelAddNewJob.UseVisualStyleBackColor = false;
            this.btnCancelAddNewJob.Click += new System.EventHandler(this.btnCancelAddNewJob_Click);
            // 
            // btnUpdateJob
            // 
            this.btnUpdateJob.BackColor = System.Drawing.SystemColors.Menu;
            this.btnUpdateJob.Location = new System.Drawing.Point(74, 105);
            this.btnUpdateJob.Name = "btnUpdateJob";
            this.btnUpdateJob.Size = new System.Drawing.Size(83, 30);
            this.btnUpdateJob.TabIndex = 3;
            this.btnUpdateJob.Text = "Zapisz";
            this.toolTip1.SetToolTip(this.btnUpdateJob, "Zapisanie zmian.");
            this.btnUpdateJob.UseVisualStyleBackColor = false;
            this.btnUpdateJob.Click += new System.EventHandler(this.btnUpdateJob_Click);
            // 
            // FormAddNewJob
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(346, 145);
            this.Controls.Add(this.btnUpdateJob);
            this.Controls.Add(this.btnCancelAddNewJob);
            this.Controls.Add(this.btnAddNewJob);
            this.Controls.Add(this.txtJobName);
            this.Controls.Add(this.lbNameOfJob);
            this.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.Name = "FormAddNewJob";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Nowe zadanie";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbNameOfJob;
        private System.Windows.Forms.TextBox txtJobName;
        private System.Windows.Forms.Button btnAddNewJob;
        private System.Windows.Forms.Button btnCancelAddNewJob;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnUpdateJob;
    }
}