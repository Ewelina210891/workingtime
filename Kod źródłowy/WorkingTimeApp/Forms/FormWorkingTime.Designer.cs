﻿namespace WorkingTimeApp
{
    partial class FormWorkingTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lbEmployeeName = new System.Windows.Forms.Label();
            this.lbNrOfProject = new System.Windows.Forms.Label();
            this.btnExitProject = new System.Windows.Forms.Button();
            this.calendar = new System.Windows.Forms.MonthCalendar();
            this.cbHoliday = new System.Windows.Forms.CheckBox();
            this.cbDayOff = new System.Windows.Forms.CheckBox();
            this.dgvJobs = new System.Windows.Forms.DataGridView();
            this.lbJobs = new System.Windows.Forms.Label();
            this.btnGenerateReport = new System.Windows.Forms.Button();
            this.lbDescriptionOfProject = new System.Windows.Forms.Label();
            this.cboxNumberOfProject = new System.Windows.Forms.ComboBox();
            this.btnAddPerformedJob = new System.Windows.Forms.Button();
            this.btnAddProject = new System.Windows.Forms.Button();
            this.btnRemovePerformedJob = new System.Windows.Forms.Button();
            this.btnEditPerformedJob = new System.Windows.Forms.Button();
            this.btnRemoveProject = new System.Windows.Forms.Button();
            this.btnEditJobs = new System.Windows.Forms.Button();
            this.btnResumeProject = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbTime = new System.Windows.Forms.Label();
            this.lbDelegation = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvJobs)).BeginInit();
            this.SuspendLayout();
            // 
            // lbEmployeeName
            // 
            this.lbEmployeeName.AutoSize = true;
            this.lbEmployeeName.Font = new System.Drawing.Font("Arial", 9F);
            this.lbEmployeeName.Location = new System.Drawing.Point(894, 19);
            this.lbEmployeeName.Name = "lbEmployeeName";
            this.lbEmployeeName.Size = new System.Drawing.Size(0, 15);
            this.lbEmployeeName.TabIndex = 4;
            // 
            // lbNrOfProject
            // 
            this.lbNrOfProject.AutoSize = true;
            this.lbNrOfProject.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lbNrOfProject.Location = new System.Drawing.Point(21, 19);
            this.lbNrOfProject.Name = "lbNrOfProject";
            this.lbNrOfProject.Size = new System.Drawing.Size(73, 15);
            this.lbNrOfProject.TabIndex = 0;
            this.lbNrOfProject.Text = "Nr projektu:";
            // 
            // btnExitProject
            // 
            this.btnExitProject.BackColor = System.Drawing.SystemColors.Menu;
            this.btnExitProject.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExitProject.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnExitProject.Location = new System.Drawing.Point(270, 68);
            this.btnExitProject.Name = "btnExitProject";
            this.btnExitProject.Size = new System.Drawing.Size(140, 30);
            this.btnExitProject.TabIndex = 7;
            this.btnExitProject.Text = "Zakończ projekt";
            this.toolTip1.SetToolTip(this.btnExitProject, "Zakończ aktualnie wybrany projekt.");
            this.btnExitProject.UseVisualStyleBackColor = false;
            this.btnExitProject.Click += new System.EventHandler(this.btnExitProject_Click);
            // 
            // calendar
            // 
            this.calendar.BackColor = System.Drawing.SystemColors.Window;
            this.calendar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.calendar.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.calendar.ForeColor = System.Drawing.SystemColors.WindowText;
            this.calendar.Location = new System.Drawing.Point(21, 131);
            this.calendar.Margin = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.calendar.MaxSelectionCount = 1;
            this.calendar.Name = "calendar";
            this.calendar.ShowTodayCircle = false;
            this.calendar.TabIndex = 9;
            this.calendar.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.calendar_DateChanged);
            // 
            // cbHoliday
            // 
            this.cbHoliday.AutoSize = true;
            this.cbHoliday.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cbHoliday.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbHoliday.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbHoliday.Location = new System.Drawing.Point(21, 299);
            this.cbHoliday.Name = "cbHoliday";
            this.cbHoliday.Size = new System.Drawing.Size(194, 19);
            this.cbHoliday.TabIndex = 10;
            this.cbHoliday.Text = "Dzień wolny od pracy (święto)";
            this.cbHoliday.UseVisualStyleBackColor = false;
            this.cbHoliday.Click += new System.EventHandler(this.cbHoliday_Click);
            // 
            // cbDayOff
            // 
            this.cbDayOff.AutoSize = true;
            this.cbDayOff.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbDayOff.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.cbDayOff.Location = new System.Drawing.Point(21, 322);
            this.cbDayOff.Name = "cbDayOff";
            this.cbDayOff.Size = new System.Drawing.Size(56, 19);
            this.cbDayOff.TabIndex = 11;
            this.cbDayOff.Text = "Urlop";
            this.cbDayOff.UseVisualStyleBackColor = true;
            this.cbDayOff.Click += new System.EventHandler(this.cbDayOff_Click);
            // 
            // dgvJobs
            // 
            this.dgvJobs.AllowUserToAddRows = false;
            this.dgvJobs.AllowUserToDeleteRows = false;
            this.dgvJobs.AllowUserToResizeRows = false;
            this.dgvJobs.BackgroundColor = System.Drawing.SystemColors.Menu;
            this.dgvJobs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvJobs.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvJobs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvJobs.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvJobs.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvJobs.Location = new System.Drawing.Point(222, 131);
            this.dgvJobs.MultiSelect = false;
            this.dgvJobs.Name = "dgvJobs";
            this.dgvJobs.ReadOnly = true;
            this.dgvJobs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvJobs.Size = new System.Drawing.Size(874, 203);
            this.dgvJobs.TabIndex = 13;
            // 
            // lbJobs
            // 
            this.lbJobs.AutoSize = true;
            this.lbJobs.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lbJobs.Location = new System.Drawing.Point(219, 110);
            this.lbJobs.Name = "lbJobs";
            this.lbJobs.Size = new System.Drawing.Size(55, 15);
            this.lbJobs.TabIndex = 12;
            this.lbJobs.Text = "Zadania:";
            // 
            // btnGenerateReport
            // 
            this.btnGenerateReport.BackColor = System.Drawing.SystemColors.Menu;
            this.btnGenerateReport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGenerateReport.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnGenerateReport.Location = new System.Drawing.Point(793, 394);
            this.btnGenerateReport.Name = "btnGenerateReport";
            this.btnGenerateReport.Size = new System.Drawing.Size(117, 30);
            this.btnGenerateReport.TabIndex = 22;
            this.btnGenerateReport.Text = "Generuj raport";
            this.toolTip1.SetToolTip(this.btnGenerateReport, "Generuj raport do pliku.");
            this.btnGenerateReport.UseVisualStyleBackColor = false;
            this.btnGenerateReport.Click += new System.EventHandler(this.btnGenerateReport_Click);
            // 
            // lbDescriptionOfProject
            // 
            this.lbDescriptionOfProject.AutoSize = true;
            this.lbDescriptionOfProject.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbDescriptionOfProject.Location = new System.Drawing.Point(174, 42);
            this.lbDescriptionOfProject.Name = "lbDescriptionOfProject";
            this.lbDescriptionOfProject.Size = new System.Drawing.Size(32, 15);
            this.lbDescriptionOfProject.TabIndex = 2;
            this.lbDescriptionOfProject.Text = "Opis";
            // 
            // cboxNumberOfProject
            // 
            this.cboxNumberOfProject.BackColor = System.Drawing.SystemColors.Menu;
            this.cboxNumberOfProject.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboxNumberOfProject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxNumberOfProject.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.cboxNumberOfProject.FormattingEnabled = true;
            this.cboxNumberOfProject.Location = new System.Drawing.Point(21, 39);
            this.cboxNumberOfProject.Name = "cboxNumberOfProject";
            this.cboxNumberOfProject.Size = new System.Drawing.Size(139, 23);
            this.cboxNumberOfProject.TabIndex = 1;
            this.toolTip1.SetToolTip(this.cboxNumberOfProject, "Wybierz numer projektu.");
            this.cboxNumberOfProject.SelectionChangeCommitted += new System.EventHandler(this.cboxNumberOfProject_SelectionChangeCommitted);
            // 
            // btnAddPerformedJob
            // 
            this.btnAddPerformedJob.BackColor = System.Drawing.SystemColors.Menu;
            this.btnAddPerformedJob.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddPerformedJob.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnAddPerformedJob.Location = new System.Drawing.Point(221, 347);
            this.btnAddPerformedJob.Name = "btnAddPerformedJob";
            this.btnAddPerformedJob.Size = new System.Drawing.Size(117, 30);
            this.btnAddPerformedJob.TabIndex = 14;
            this.btnAddPerformedJob.Text = "Dodaj";
            this.toolTip1.SetToolTip(this.btnAddPerformedJob, "Dodaj nowe wykonane zadanie.");
            this.btnAddPerformedJob.UseVisualStyleBackColor = false;
            this.btnAddPerformedJob.Click += new System.EventHandler(this.btnAddPerformedJob_Click);
            // 
            // btnAddProject
            // 
            this.btnAddProject.BackColor = System.Drawing.SystemColors.Menu;
            this.btnAddProject.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddProject.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnAddProject.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnAddProject.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnAddProject.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnAddProject.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAddProject.Location = new System.Drawing.Point(21, 68);
            this.btnAddProject.Name = "btnAddProject";
            this.btnAddProject.Size = new System.Drawing.Size(117, 30);
            this.btnAddProject.TabIndex = 5;
            this.btnAddProject.Text = "Dodaj projekt";
            this.toolTip1.SetToolTip(this.btnAddProject, "Dodaj nowy projekt.");
            this.btnAddProject.UseVisualStyleBackColor = false;
            this.btnAddProject.Click += new System.EventHandler(this.btnAddProject_Click);
            // 
            // btnRemovePerformedJob
            // 
            this.btnRemovePerformedJob.BackColor = System.Drawing.SystemColors.MenuBar;
            this.btnRemovePerformedJob.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRemovePerformedJob.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnRemovePerformedJob.Location = new System.Drawing.Point(469, 347);
            this.btnRemovePerformedJob.Name = "btnRemovePerformedJob";
            this.btnRemovePerformedJob.Size = new System.Drawing.Size(117, 30);
            this.btnRemovePerformedJob.TabIndex = 16;
            this.btnRemovePerformedJob.Text = "Usuń";
            this.toolTip1.SetToolTip(this.btnRemovePerformedJob, "Usuń wybrane zakończone zadanie.");
            this.btnRemovePerformedJob.UseVisualStyleBackColor = false;
            this.btnRemovePerformedJob.Click += new System.EventHandler(this.btnRemovePerformedJob_Click);
            // 
            // btnEditPerformedJob
            // 
            this.btnEditPerformedJob.BackColor = System.Drawing.SystemColors.Menu;
            this.btnEditPerformedJob.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEditPerformedJob.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnEditPerformedJob.Location = new System.Drawing.Point(345, 347);
            this.btnEditPerformedJob.Name = "btnEditPerformedJob";
            this.btnEditPerformedJob.Size = new System.Drawing.Size(117, 30);
            this.btnEditPerformedJob.TabIndex = 15;
            this.btnEditPerformedJob.Text = "Edytuj";
            this.toolTip1.SetToolTip(this.btnEditPerformedJob, "Edytuj wybrane wykonane zadanie.");
            this.btnEditPerformedJob.UseVisualStyleBackColor = false;
            this.btnEditPerformedJob.Click += new System.EventHandler(this.btnEditPerformedJob_Click);
            // 
            // btnRemoveProject
            // 
            this.btnRemoveProject.BackColor = System.Drawing.SystemColors.Menu;
            this.btnRemoveProject.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRemoveProject.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnRemoveProject.Location = new System.Drawing.Point(145, 68);
            this.btnRemoveProject.Name = "btnRemoveProject";
            this.btnRemoveProject.Size = new System.Drawing.Size(117, 30);
            this.btnRemoveProject.TabIndex = 6;
            this.btnRemoveProject.Text = "Usuń projekt";
            this.toolTip1.SetToolTip(this.btnRemoveProject, "Usuń aktualnie wybrany projekt.");
            this.btnRemoveProject.UseVisualStyleBackColor = false;
            this.btnRemoveProject.Click += new System.EventHandler(this.btnRemoveProject_Click);
            // 
            // btnEditJobs
            // 
            this.btnEditJobs.BackColor = System.Drawing.SystemColors.Menu;
            this.btnEditJobs.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEditJobs.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnEditJobs.Location = new System.Drawing.Point(309, 394);
            this.btnEditJobs.Name = "btnEditJobs";
            this.btnEditJobs.Size = new System.Drawing.Size(175, 30);
            this.btnEditJobs.TabIndex = 17;
            this.btnEditJobs.Text = "Edytuj dostępne zadania";
            this.toolTip1.SetToolTip(this.btnEditJobs, "Dodaj/usuń dostepne zadania.");
            this.btnEditJobs.UseVisualStyleBackColor = false;
            this.btnEditJobs.Click += new System.EventHandler(this.btnEditJobs_Click);
            // 
            // btnResumeProject
            // 
            this.btnResumeProject.BackColor = System.Drawing.SystemColors.Menu;
            this.btnResumeProject.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnResumeProject.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnResumeProject.Location = new System.Drawing.Point(271, 68);
            this.btnResumeProject.Name = "btnResumeProject";
            this.btnResumeProject.Size = new System.Drawing.Size(140, 30);
            this.btnResumeProject.TabIndex = 8;
            this.btnResumeProject.Text = "Wznów projekt";
            this.toolTip1.SetToolTip(this.btnResumeProject, "Wznów aktualnie wybrany projekt.");
            this.btnResumeProject.UseVisualStyleBackColor = false;
            this.btnResumeProject.Visible = false;
            this.btnResumeProject.Click += new System.EventHandler(this.btnResumeProject_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(790, 347);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(227, 15);
            this.label1.TabIndex = 18;
            this.label1.Text = "Przepracowane godziny w danym dniu:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(790, 370);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 15);
            this.label2.TabIndex = 20;
            this.label2.Text = "W tym delegacja: ";
            // 
            // lbTime
            // 
            this.lbTime.AutoSize = true;
            this.lbTime.Font = new System.Drawing.Font("Arial", 9F);
            this.lbTime.Location = new System.Drawing.Point(1028, 347);
            this.lbTime.Name = "lbTime";
            this.lbTime.Size = new System.Drawing.Size(0, 15);
            this.lbTime.TabIndex = 19;
            // 
            // lbDelegation
            // 
            this.lbDelegation.AutoSize = true;
            this.lbDelegation.Font = new System.Drawing.Font("Arial", 9F);
            this.lbDelegation.Location = new System.Drawing.Point(1028, 370);
            this.lbDelegation.Name = "lbDelegation";
            this.lbDelegation.Size = new System.Drawing.Size(0, 15);
            this.lbDelegation.TabIndex = 21;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(790, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "Imię i nazwisko:";
            // 
            // FormWorkingTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1114, 442);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbDelegation);
            this.Controls.Add(this.lbTime);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnResumeProject);
            this.Controls.Add(this.btnEditJobs);
            this.Controls.Add(this.btnRemoveProject);
            this.Controls.Add(this.btnEditPerformedJob);
            this.Controls.Add(this.btnRemovePerformedJob);
            this.Controls.Add(this.btnAddProject);
            this.Controls.Add(this.btnAddPerformedJob);
            this.Controls.Add(this.cboxNumberOfProject);
            this.Controls.Add(this.lbDescriptionOfProject);
            this.Controls.Add(this.btnGenerateReport);
            this.Controls.Add(this.lbJobs);
            this.Controls.Add(this.dgvJobs);
            this.Controls.Add(this.cbDayOff);
            this.Controls.Add(this.cbHoliday);
            this.Controls.Add(this.calendar);
            this.Controls.Add(this.btnExitProject);
            this.Controls.Add(this.lbNrOfProject);
            this.Controls.Add(this.lbEmployeeName);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.MaximumSize = new System.Drawing.Size(1130, 600);
            this.MinimumSize = new System.Drawing.Size(1130, 450);
            this.Name = "FormWorkingTime";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Czas Pracy";
            this.Activated += new System.EventHandler(this.FormWorkingTime_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.dgvJobs)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbEmployeeName;
        private System.Windows.Forms.Label lbNrOfProject;
        private System.Windows.Forms.Button btnExitProject;
        private System.Windows.Forms.MonthCalendar calendar;
        private System.Windows.Forms.CheckBox cbHoliday;
        private System.Windows.Forms.CheckBox cbDayOff;
        private System.Windows.Forms.DataGridView dgvJobs;
        private System.Windows.Forms.Label lbJobs;
        private System.Windows.Forms.Button btnGenerateReport;
        private System.Windows.Forms.Label lbDescriptionOfProject;
        private System.Windows.Forms.ComboBox cboxNumberOfProject;
        private System.Windows.Forms.Button btnAddPerformedJob;
        private System.Windows.Forms.Button btnAddProject;
        private System.Windows.Forms.Button btnRemovePerformedJob;
        private System.Windows.Forms.Button btnEditPerformedJob;
        private System.Windows.Forms.Button btnRemoveProject;
        private System.Windows.Forms.Button btnEditJobs;
        private System.Windows.Forms.Button btnResumeProject;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbTime;
        private System.Windows.Forms.Label lbDelegation;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}

