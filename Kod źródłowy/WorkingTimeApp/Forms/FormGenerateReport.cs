﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WorkingTimeApp.ExportData;
using WorkingTimeApp.Repositories;

namespace WorkingTimeApp
{
    public partial class FormGenerateReport : Form
    {
        IRepository _repository;

        public FormGenerateReport(IRepository sqlRepository)
        {
            _repository = sqlRepository;

            InitializeComponent();

            cbTypeOfReport.SelectedItem = cbTypeOfReport.Items[0];
            rbReportForProject.Checked = true;
            rbReportForRange.Checked = false;
            mcRangeOfDays.Enabled = false;

            GetNumbersOfProjects();
        }

        private void GetNumbersOfProjects()
        {
            var result = _repository.GetProjects();

            cbChooseProject.DataSource = result;
            cbChooseProject.DisplayMember = "ProjectNumber";
            cbChooseProject.ValueMember = "Description";

            cbChooseProject.SelectedIndex = 0;
        }

        private void rbReportForProject_CheckedChanged(object sender, EventArgs e)
        {
            if (rbReportForProject.Checked)
            {
                cbChooseProject.Enabled = true;
                mcRangeOfDays.Enabled = false;
            }
            else
            {
                cbChooseProject.Enabled = false;
                mcRangeOfDays.Enabled = true;
            }
        }

        private void btnSaveReport_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfdSaveReportToFile = new SaveFileDialog();

            sfdSaveReportToFile.Filter = "Excel File|*.xlsx";
            sfdSaveReportToFile.Title = "Zapis do pliku Excel";

            if (sfdSaveReportToFile.ShowDialog() == DialogResult.OK)
            {
                GenerateReport(sfdSaveReportToFile.FileName);           
            }
        }

        public void GenerateReport(string fileName)
        {
            var excelExport = new ExcelExport(_repository);

            if (cbTypeOfReport.SelectedIndex == 0)
            {
                if (rbReportForProject.Checked)
                {
                    excelExport.GenerateGeneralReportForOneProject(fileName, cbChooseProject.GetItemText(cbChooseProject.SelectedItem));
                    MessageBox.Show("Wygenerowano");
                }
                if (rbReportForRange.Checked)
                {
                    excelExport.GenerateGeneralReportForRange(fileName, mcRangeOfDays.SelectionStart, mcRangeOfDays.SelectionEnd);
                    MessageBox.Show("Wygenerowano");
                }
            }
            if (cbTypeOfReport.SelectedIndex == 1)
            {                
                if (rbReportForProject.Checked)
                {
                    excelExport.GenerateDayToDayReporForOneProject(fileName, cbChooseProject.GetItemText(cbChooseProject.SelectedItem));
                    MessageBox.Show("Wygenerowano");
                }
                if (rbReportForRange.Checked)
                {
                    excelExport.GenerateDayToDayReportForRange(fileName, mcRangeOfDays.SelectionStart, mcRangeOfDays.SelectionEnd);
                    MessageBox.Show("Wygenerowano");
                }
            }
        }
    }
}
