﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WorkingTimeApp.Repositories;

namespace WorkingTimeApp
{
    public partial class FormAddNewProject : Form
    {
        private IRepository _repository;

        public FormAddNewProject(IRepository repository)
        {
            _repository = repository;
            InitializeComponent();
        }


        private void rtxtDescriptionOfProject_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                AddNewProject();
            }
        }


        private void btnAddNewProject_Click(object sender, EventArgs e)
        {
            AddNewProject();
        }
        private void AddNewProject()
        {
            if (txtProjectName.TextLength < 5 || rtxtDescriptionOfProject.Text == "")
            {
                MessageBox.Show("Nie można dodać projektu. Nr projektu musi zawierać co najmniej 5 znaków!");
            }
            else
            {
                try
                {
                    _repository.AddProject(txtProjectName.Text, rtxtDescriptionOfProject.Text);
                    MessageBox.Show("Dodano nowy projekt");
                    this.Close();
                }
                catch
                {
                    MessageBox.Show("Dodanie nowego projektu nie powiodło się");
                }
            }
        }

        private void btnCancelAddProject_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
