﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WorkingTimeApp.Repositories;

namespace WorkingTimeApp
{
    public partial class FormJobs : Form
    {
        private IRepository _repository;

        public FormJobs(IRepository repository)
        {
            _repository = repository;
            InitializeComponent();
        }


        private void FormJobs_Activated(object sender, EventArgs e)
        {
            GetAllJobs();
        }

        private void GetAllJobs()
        {
            var result = _repository.GetAllJobs();
            dgvAllJobs.DataSource = result;
            dgvAllJobs.Columns["JobId"].Visible = false;
            dgvAllJobs.Columns["Description"].Width = 400;
            dgvAllJobs.Columns["Description"].HeaderText = "Nazwa zadania";
        }     


        private void btnAddJob_Click(object sender, EventArgs e)
        {
            var jobs = new FormAddNewJob(_repository);
            jobs.Show();
        }

        private void btnEditJob_Click(object sender, EventArgs e)
        {

            if (dgvAllJobs.SelectedRows.Count == 1)
            {
                DataGridViewCellCollection wiersz = dgvAllJobs.SelectedRows[0].Cells;

                var jobId = Convert.ToInt32(wiersz[0].Value.ToString());
                var jobDescription = wiersz[1].Value.ToString();

                var jobs = new FormAddNewJob(_repository, jobId, jobDescription);
                jobs.Show();

            }
        }

        private void btnDeleteJob_Click(object sender, EventArgs e)
        {
            if (dgvAllJobs.SelectedRows.Count == 1)
            {
                var dialogResult = MessageBox.Show("Czy na pewno chcesz usunąć to zadanie? Zostaną usunięte wszystkie wykonane zadania powiązane z tym zadaniem.", "Potwierdzenie usunięcia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    DataGridViewCellCollection wiersz = dgvAllJobs.SelectedRows[0].Cells;

                    var jobId = Convert.ToInt32(wiersz[0].Value.ToString());
                    _repository.RemoveJob(jobId);
                    _repository.RemovePerformedJobsForJob(jobId);
                    GetAllJobs();
                    MessageBox.Show("Usunięto zadanie");
                }
            }
        }

        private void btnCancelSavingJobs_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
