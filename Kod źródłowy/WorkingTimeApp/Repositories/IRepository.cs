﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkingTimeApp.Repositories
{
    public interface IRepository
    {
        #region get projects
        DataTable GetProjects();
        DataTable GetProjectsForRange(DateTime startDate, DateTime endDate);
        #endregion

        #region get jobs
        DataTable GetJob(int jobId);
        DataTable GetJobsWithoutPerformedJobs(string projectNumber, DateTime date);
        DataTable GetAllJobs();
        #endregion

        #region get performed jobs
        DataTable GetPerformedJobsForOneProjectOnOneDay(DateTime date, string projectNumber);
        DataTable GetPerformedJobsForAllProjectsOnOneDay(DateTime date);
        #endregion

        #region get time of performed jobs
        DataTable GetTimeOfPerformedJobsInOneDay(DateTime date, int performedJobId);
        DataTable GetJobsAndTimeForProjectForRange(string projectName, DateTime startDate, DateTime endDate);
        DataTable GetAllJobsAndTimeForProject(string projectName);
        #endregion

        #region get working days
        DataTable GetWorkingDaysForOneProject(string projectName);
        #endregion

        #region get actual working time
        int GetWorkTimeInActualDayForAllProjects(DateTime actualDate);
        int GetDelegationTimeInActualDayForAllProjects(DateTime actualDate);

        int GetWorkTimeInActualDayForOneProject(DateTime actualDate, string projectNumber);
        int GetDelegationTimeInActualDayForOneProject(DateTime actualDate, string projectNumber);
        #endregion

        #region project
        void AddProject(string projectName, string projectDescription);
        void CancelProject(string projectNumber);
        void ResumeProject(string projectNumber);
        void RemoveProject(string projectNumber);
        #endregion

        #region performed job
        void AddPerformedJob(PerformedJobItem performedJob);
        void UpdatePerformedJob(PerformedJobItem performedJob);
        void RemovePerformedJob(int performedJobId);
        void RemovePerformedJobsFromProject(string projectNumber);
        void RemovePerformedJobsForJob(int jobId);
        #endregion

        #region job
        void AddJob(string jobDescription);
        void UpdateJob(int jobId, string jobDescription);
        void RemoveJob(int jobId);

        #endregion

        #region holiday
        void AddHoliday(DateTime freeDay);
        void RemoveHoliday(DateTime freeDay);
        #endregion

        #region day off
        void AddDayOff(DateTime freeDay);
        void RemoveDayOff(DateTime freeDay);
        #endregion

        #region checking day
        bool CheckThatActualDayIsHoliday(DateTime actualDay);
        bool CheckThatActualDayIsDayOff(DateTime actualDay);
        bool CheckThatActualDayIsDelegation(DateTime actualDay);
        #endregion

        #region checking project
        bool CheckThatProjectIsCancel(string projectNumber);
        #endregion

        #region generate report
        int GetDaysOfWorkForRange(DateTime startDate, DateTime endDate);

        int GetWorkingMinutesForOneProject(string projectName);
        int GetWorkingMinutesForRange(DateTime startDate, DateTime endDate);

        int GetWorkingProjectMinutesForOneProject(string projectName);
        int GetWorkingProjectMinutesForRange(DateTime startDate, DateTime endDate);

        int GetWorkingAdministrationMinutesForOneProject(string projectName);
        int GetWorkingAdministrationMinutesForRange(DateTime startDate, DateTime endDate);

        int GetDayOffHoursForRange(DateTime startDate, DateTime endDate);
        int GetWeekendMinutesForRange(DateTime startDate, DateTime endDate);
        #endregion
    }
}
