﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data;
using WorkingTimeApp.Repositories;
using System.Windows.Forms;

namespace WorkingTimeApp
{
    class SqlRepository : IRepository
    {
        private SqlConnection _connection; 

        public SqlRepository(string instance)
        {
            _connection = new SqlConnection();
            _connection.ConnectionString = String.Format(@"Data Source={0};database=WorkingTime;Trusted_Connection=True;", instance); 
        }

        #region get projects
        public DataTable GetProjects()
        {
            var query = "SELECT ProjectNumber,Description FROM Projects";
            var dataTableResult = DatabaseQueryWithReturnValue(query);
            return dataTableResult;
        }

        public DataTable GetProjectsForRange(DateTime startDate, DateTime endDate)
        {
            var query = String.Format("SELECT ProjectNumber From PerformedJobs WHERE Date Between '{0}' AND '{1}' GROUP BY  ProjectNumber", startDate.ToShortDateString(), endDate.ToShortDateString());

            var dataTableResult = DatabaseQueryWithReturnValue(query);
            return dataTableResult;
        }
        #endregion

        #region get jobs
        public DataTable GetJob(int jobId)
        {
            var query = String.Format("SELECT Description, JobId FROM Jobs WHERE JobId='{0}'", jobId);
            var dataTableResult = DatabaseQueryWithReturnValue(query);
            return dataTableResult;
        }
        public DataTable GetJobsWithoutPerformedJobs(string projectNumber, DateTime date)
        {
            var query = String.Format("SELECT Jobs.Description, Jobs.JobId FROM Jobs WHERE JobId NOT IN(SELECT JobId FROM PerformedJobs WHERE PerformedJobs.ProjectNumber= '{0}' AND PerformedJobs.Date='{1}')", projectNumber, date.ToShortDateString());
            var dataTableResult = DatabaseQueryWithReturnValue(query);
            return dataTableResult;
        }
        public DataTable GetAllJobs()
        {
            var query = String.Format("SELECT JobId, Description FROM Jobs ORDER BY Description");
            var dataTableResult = DatabaseQueryWithReturnValue(query);
            return dataTableResult;
        }
        #endregion

        #region get performed jobs
        public DataTable GetPerformedJobsForOneProjectOnOneDay(DateTime date, string projectNumber)
        {
            var query = String.Format("SELECT PerformedJobs.PerformedJobId, PerformedJobs.JobId as JobId, PerformedJobs.ProjectNumber, Jobs.Description, LEFT(PerformedJobs.StartTime,5) as StartTime,LEFT(PerformedJobs.EndTime,5) as EndTime,PerformedJobs.Delegation, PerformedJobs.Comment as Comment FROM Jobs INNER JOIN PerformedJobs ON Jobs.JobId=PerformedJobs.JobId WHERE PerformedJobs.Date='{0}' AND PerformedJobs.ProjectNumber='{1}' Order By Date, StartTime", date.ToShortDateString(), projectNumber);
            var dataTableResult = DatabaseQueryWithReturnValue(query);
            return dataTableResult;
        }
        public DataTable GetPerformedJobsForAllProjectsOnOneDay(DateTime date)
        {
            var query = String.Format("SELECT PerformedJobs.PerformedJobId, PerformedJobs.JobId, PerformedJobs.ProjectNumber, Jobs.Description, LEFT(PerformedJobs.StartTime,5) as StartTime,LEFT(PerformedJobs.EndTime,5) as EndTime,PerformedJobs.Delegation, PerformedJobs.Comment FROM Jobs INNER JOIN PerformedJobs ON Jobs.JobId=PerformedJobs.JobId WHERE PerformedJobs.Date='{0}'", date.ToShortDateString());
            var dataTableResult = DatabaseQueryWithReturnValue(query);
            return dataTableResult;
        }
        #endregion

        #region get time of performed jobs
        public DataTable GetTimeOfPerformedJobsInOneDay(DateTime date, int performedJobId)
        {
            var query = String.Format("SELECT StartTime, EndTime FROM PerformedJobs WHERE Date='{0}' AND PerformedJobId!={1} ORDER BY StartTime", date.ToShortDateString(), performedJobId);
            var dataTableResult = DatabaseQueryWithReturnValue(query);
            return dataTableResult;
        }
        public DataTable GetJobsAndTimeForProjectForRange(string projectName, DateTime startDate, DateTime endDate)
        {
            var query = String.Format("Select JobTimes.Description, Sum(JobTimes.Time) From(Select Jobs.Description, Sum(datediff(mi, PerformedJobs.StartTime, PerformedJobs.EndTime)) as Time From dbo.Jobs Inner join dbo.PerformedJobs on Jobs.JobId=PerformedJobs.JobId Where PerformedJobs.Date Between '{0}' And '{1}' AND PerformedJobs.ProjectNumber='{2}' group by Description Union ( Select Jobs.Description, Sum(DateDiff(mi, PerformedJobs.StartTime, PerformedJobs.EndTime)*2) as Time From dbo.Jobs Inner join dbo.PerformedJobs on Jobs.JobId=PerformedJobs.JobId right join dbo.FreeDays on FreeDays.Date=PerformedJobs.Date Where PerformedJobs.Date Between '{0}' And '{1}' AND PerformedJobs.ProjectNumber='{2}' Group by Jobs.Description)) as JobTimes Group by JobTimes.Description", startDate.ToShortDateString(), endDate.ToShortDateString(), projectName);

            var dataTableResult = DatabaseQueryWithReturnValue(query);
            return dataTableResult;
        }
        public DataTable GetAllJobsAndTimeForProject(string projectName)
        {
            var query = String.Format("SELECT Jobs.Description, SUM(DATEDIFF(mi, StartTime, EndTime)) as Time From PerformedJobs INNER JOIN Jobs ON PerformedJobs.JobId=Jobs.JobId WHERE ProjectNumber='{0}' GROUP BY  Jobs.Description", projectName);

            var dataTableResult = DatabaseQueryWithReturnValue(query);
            return dataTableResult;
        }
        #endregion

        #region get working days
        public DataTable GetWorkingDaysForOneProject(string projectName)
        {
            var query = String.Format("SELECT Date From PerformedJobs WHERE ProjectNumber='{0}' GROUP BY  Date", projectName);

            var dataTableResult = DatabaseQueryWithReturnValue(query);
            return dataTableResult;
        }   
        #endregion

        #region get actual working time
        public int GetWorkTimeInActualDayForOneProject(DateTime actualDate, string projectNumber)
        {
            var query = String.Format("SELECT SUM(DATEDIFF(mi, StartTime, EndTime) )as Time FROM dbo.PerformedJobs WHERE Date='{0}' AND ProjectNumber='{1}'", actualDate.ToShortDateString(), projectNumber);
            var dataTableResult = DatabaseQueryWithReturnValue(query);

            DataRow dataRow = dataTableResult.Rows[0];
            var dataRowValue = dataRow[0];

            if (dataRowValue != null & dataRowValue.ToString() != "")
            {
                return Convert.ToInt32(dataRowValue.ToString());
            }
            else
                return 0;

        }
        public int GetDelegationTimeInActualDayForOneProject(DateTime actualDate, string projectNumber)
        {
            var query = String.Format("SELECT SUM(DATEDIFF(mi, StartTime, EndTime) )as Time FROM dbo.PerformedJobs WHERE Date='{0}' AND Delegation='true' AND ProjectNumber='{1}'", actualDate.ToShortDateString(), projectNumber);
            var dataTableResult = DatabaseQueryWithReturnValue(query);

            DataRow dataRow = dataTableResult.Rows[0];
            var dataRowValue = dataRow[0];

            if (dataRowValue != null & dataRowValue.ToString() != "")
            {
                return Convert.ToInt32(dataRowValue.ToString());
            }
            else
                return 0;

        }

        public int GetWorkTimeInActualDayForAllProjects(DateTime actualDate)
        {
            var query = String.Format("SELECT SUM(DATEDIFF(mi, StartTime, EndTime) )as Time FROM dbo.PerformedJobs WHERE Date='{0}'", actualDate.ToShortDateString());
            var dataTableResult = DatabaseQueryWithReturnValue(query);

            DataRow dataRow = dataTableResult.Rows[0];
            var dataRowValue = dataRow[0];

            if (dataRowValue != null & dataRowValue.ToString() != "")
            {
                return Convert.ToInt32(dataRowValue.ToString());
            }
            else
                return 0;

        }
        public int GetDelegationTimeInActualDayForAllProjects(DateTime actualDate)
        {
            var query = String.Format("SELECT SUM(DATEDIFF(mi, StartTime, EndTime) )as Time FROM dbo.PerformedJobs WHERE Date='{0}' AND Delegation='true'", actualDate);
            var dataTableResult = DatabaseQueryWithReturnValue(query);

            DataRow dataRow = dataTableResult.Rows[0];
            var dataRowValue = dataRow[0];

            if (dataRowValue != null & dataRowValue.ToString() != "")
            {
                return Convert.ToInt32(dataRowValue.ToString());
            }
            else
                return 0;

        }
        #endregion

        #region project
        public void AddProject(string projectName, string projectDescription)
        {
            var query = String.Format("INSERT INTO dbo.Projects (ProjectNumber, Description, IsCancel) VALUES ( '{0}', '{1}', 'false')", projectName, projectDescription);
            DatabaseQueryWithoutReturnValue(query);
        }
        public void CancelProject(string projectNumber)
        {
            var query = String.Format("UPDATE dbo.Projects SET IsCancel='1' WHERE ProjectNumber='{0}'", projectNumber);
            DatabaseQueryWithoutReturnValue(query);
        }
        public void ResumeProject(string projectNumber)
        {
            var query = String.Format("UPDATE dbo.Projects SET IsCancel='0' WHERE ProjectNumber='{0}'", projectNumber);
            DatabaseQueryWithoutReturnValue(query);
        }
        public void RemoveProject(string projectNumber)
        {
            var query = String.Format("DELETE FROM dbo.Projects WHERE ProjectNumber='{0}'", projectNumber);
            DatabaseQueryWithoutReturnValue(query);
        }
        #endregion

        #region performed job
        public void AddPerformedJob(PerformedJobItem performedJob)
        {

            var query = String.Format("INSERT INTO dbo.PerformedJobs (ProjectNumber,JobId, Date, StartTime, EndTime,Delegation, Comment) VALUES ( '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')", performedJob.ProjectNumber, performedJob.JobId, performedJob.Date, performedJob.StartTime.ToShortTimeString(), performedJob.EndTime.ToShortTimeString(), performedJob.Delegation, performedJob.Comment, performedJob.PerformedJobId);
            DatabaseQueryWithoutReturnValue(query);
        }
        public void UpdatePerformedJob(PerformedJobItem performedJob)
        {
            var query = String.Format("UPDATE dbo.PerformedJobs SET ProjectNumber='{0}',JobId='{1}', Date='{2}',StartTime='{3}', EndTime='{4}',Delegation='{5}', Comment='{6}'  WHERE PerformedJobId ='{7}'", performedJob.ProjectNumber, performedJob.JobId, performedJob.Date, performedJob.StartTime.ToShortTimeString(), performedJob.EndTime.ToShortTimeString(), performedJob.Delegation, performedJob.Comment, performedJob.PerformedJobId);
            DatabaseQueryWithoutReturnValue(query);
        }
        public void RemovePerformedJob(int performedJobId)
        {
            var query = String.Format("DELETE FROM dbo.PerformedJobs WHERE PerformedJobId='{0}'", performedJobId);
            DatabaseQueryWithoutReturnValue(query);
        }
        public void RemovePerformedJobsFromProject(string projectNumber)
        {
            var query = String.Format("DELETE FROM dbo.PerformedJobs WHERE ProjectNumber='{0}'", projectNumber);
            DatabaseQueryWithoutReturnValue(query);
        }
        public void RemovePerformedJobsForJob(int jobId)
        {
            var query = String.Format("DELETE FROM dbo.PerformedJobs WHERE JobId='{0}'", jobId);
            DatabaseQueryWithoutReturnValue(query);
        }
        #endregion

        #region job
        public void AddJob(string jobDescription)
        {
            var query = String.Format("INSERT INTO dbo.Jobs ( Description) VALUES ( '{0}')", jobDescription);
            DatabaseQueryWithoutReturnValue(query);
        }
        public void UpdateJob(int jobId, string jobDescription)
        {
            var query = String.Format("UPDATE dbo.Jobs SET Description='{0}' WHERE jobId ='{1}'", jobDescription, jobId);
            DatabaseQueryWithoutReturnValue(query);
        }
        public void RemoveJob(int jobId)
        {
            var query = String.Format("DELETE FROM dbo.Jobs WHERE jobId='{0}'", jobId);
            DatabaseQueryWithoutReturnValue(query);
        }
        #endregion

        #region holiday
        public void AddHoliday(DateTime freeDay)
        {
            var query = String.Format("INSERT INTO dbo.FreeDays (Date, Type) VALUES ( '{0}', 'holiday' )", freeDay);
            DatabaseQueryWithoutReturnValue(query);

        }
        public void RemoveHoliday(DateTime freeDay)
        {
            var query = String.Format("DELETE FROM dbo.FreeDays WHERE Date='{0}' AND Type='holiday'", freeDay);
            DatabaseQueryWithoutReturnValue(query);
        }
        #endregion

        #region day off
        public void AddDayOff(DateTime freeDay)
        {
            var query = String.Format("INSERT INTO dbo.FreeDays (Date, Type) VALUES ( '{0}', 'day off' )", freeDay);
            DatabaseQueryWithoutReturnValue(query);

        }
        public void RemoveDayOff(DateTime freeDay)
        {
            var query = String.Format("DELETE FROM dbo.FreeDays WHERE Date='{0}' AND Type='day off'", freeDay);
            DatabaseQueryWithoutReturnValue(query);
        }
        #endregion

        #region checking day
        public bool CheckThatActualDayIsHoliday(DateTime actualDay)
        {
            var query = String.Format("SELECT COUNT(*) FROM dbo.FreeDays WHERE Date='{0}' AND Type='holiday' GROUP BY Date", actualDay.ToShortDateString());
            var dataTableResult = DatabaseQueryWithReturnValue(query);

            if (dataTableResult.Rows.Count == 0)
                return false;
            else
                return true;
        }
        public bool CheckThatActualDayIsDayOff(DateTime actualDay)
        {
            var query = String.Format("SELECT COUNT(*) FROM dbo.FreeDays WHERE Date='{0}' AND Type='day off' GROUP BY Date", actualDay.ToShortDateString());
            var dataTableResult = DatabaseQueryWithReturnValue(query);

            if (dataTableResult.Rows.Count == 0)
                return false;
            else
                return true;
        }
        public bool CheckThatActualDayIsDelegation(DateTime actualDay)
        {
            var query = String.Format("SELECT COUNT(*) FROM dbo.FreeDays WHERE Date='{0}' AND Type='delegation' GROUP BY Date", actualDay.ToShortDateString());
            var dataTableResult = DatabaseQueryWithReturnValue(query);

            if (dataTableResult.Rows.Count == 0)
                return false;
            else
                return true;
        }
        #endregion

        #region checking project
        public bool CheckThatProjectIsCancel(string projectNumber)
        {
            var query = String.Format("SELECT COUNT(*) FROM dbo.Projects WHERE ProjectNumber='{0}' AND IsCancel='true' GROUP BY ProjectNumber", projectNumber);
            var dataTableResult = DatabaseQueryWithReturnValue(query);

            if (dataTableResult.Rows.Count == 0)
                return false;
            else
                return true;
        }
        #endregion

        #region generate report
        public int GetDaysOfWorkForRange(DateTime startDate, DateTime endDate)
        {
            var query = String.Format("SELECT DATEDIFF(dd, '{0}', '{1}') + 1 - (SELECT COUNT(*) FROM dbo.FreeDays WHERE Date BETWEEN '{0}' AND '{1}' AND Type='holiday')", startDate.ToShortDateString(), endDate.ToShortDateString());
            var dataTableResult = DatabaseQueryWithReturnValue(query);

            return GetIntValueFromResult(dataTableResult);

        }

        public int GetWorkingMinutesForOneProject(string projectName)
        {

            var query = String.Format("SELECT SUM(DATEDIFF(mi, StartTime, EndTime) )as Time FROM dbo.PerformedJobs WHERE ProjectNumber='{0}' ", projectName);
            var dataTableResult = DatabaseQueryWithReturnValue(query);

            return GetIntValueFromResult(dataTableResult);
        }
        public int GetWorkingMinutesForRange(DateTime startDate, DateTime endDate)
        {

            var query = String.Format("SELECT SUM(DATEDIFF(mi, StartTime, EndTime) )as Time FROM dbo.PerformedJobs WHERE Date BETWEEN '{0}' AND '{1}' ", startDate.ToShortDateString(), endDate.ToShortDateString());
            var dataTableResult = DatabaseQueryWithReturnValue(query);

            return GetIntValueFromResult(dataTableResult);
        }

        public int GetWorkingProjectMinutesForOneProject(string projectName)
        {
            var query = String.Format("SELECT SUM(DATEDIFF(mi, StartTime, EndTime)) as Time FROM dbo.PerformedJobs WHERE ProjectNumber='{0}' AND ProjectNumber!='Czas admin. '", projectName);
            var dataTableResult = DatabaseQueryWithReturnValue(query);

            return GetIntValueFromResult(dataTableResult);
        }
        public int GetWorkingProjectMinutesForRange(DateTime startDate, DateTime endDate)
        {
            var query = String.Format("SELECT SUM(DATEDIFF(mi, StartTime, EndTime)) as Time FROM dbo.PerformedJobs WHERE Date BETWEEN '{0}' AND '{1}' AND ProjectNumber!='Czas admin. '", startDate.ToShortDateString(), endDate.ToShortDateString());
            var dataTableResult = DatabaseQueryWithReturnValue(query);

            return GetIntValueFromResult(dataTableResult);
        }

        public int GetWorkingAdministrationMinutesForOneProject(string projectName)
        {
            var query = String.Format("SELECT SUM(DATEDIFF(mi, StartTime, EndTime) ) as Time FROM dbo.PerformedJobs WHERE ProjectNumber='{0}' AND ProjectNumber='Czas admin. '", projectName);
            var dataTableResult = DatabaseQueryWithReturnValue(query);

            return GetIntValueFromResult(dataTableResult);
        }
        public int GetWorkingAdministrationMinutesForRange(DateTime startDate, DateTime endDate)
        {
            var query = String.Format("SELECT SUM(DATEDIFF(mi, StartTime, EndTime) ) as Time FROM dbo.PerformedJobs WHERE Date BETWEEN '{0}' AND '{1}' AND ProjectNumber='Czas admin. '", startDate.ToShortDateString(), endDate.ToShortDateString());
            var dataTableResult = DatabaseQueryWithReturnValue(query);

            return GetIntValueFromResult(dataTableResult);
        }

        public int GetDayOffHoursForRange(DateTime startDate, DateTime endDate)
        {
            var query = String.Format("SELECT COUNT(*)*8 as Time FROM dbo.FreeDays WHERE Date BETWEEN '{0}' AND '{1}' AND Type='day off'", startDate.ToShortDateString(), endDate.ToShortDateString());
            var dataTableResult = DatabaseQueryWithReturnValue(query);

            return GetIntValueFromResult(dataTableResult);
        }
        public int GetWeekendMinutesForRange(DateTime startDate, DateTime endDate)
        {
            var query = String.Format("Select Sum(DATEDIFF(mi, StartTime, EndTime)) as Time From dbo.PerformedJobs Where Date in( Select Date  FROM dbo.FreeDays  WHERE Date BETWEEN '{0}' AND '{1}' AND Type ='holiday')", startDate.ToShortDateString(), endDate.ToShortDateString());
            var dataTableResult = DatabaseQueryWithReturnValue(query);

            return GetIntValueFromResult(dataTableResult);
        }

        #endregion

        #region execute query
        private void DatabaseQueryWithoutReturnValue(string query)
        {
            _connection.Open();

            var command = buildSqlCommand(query);
            executeCommandWithoutReturnValue(command);

            _connection.Close();
        }
        private DataTable DatabaseQueryWithReturnValue(string query)
        {
            _connection.Open();

            var command = buildSqlCommand(query);
            var dataReader = executeCommandWithReturnValue(command);
            var dataTableResult = buildDataTableResult(dataReader);

            _connection.Close();

            return dataTableResult;
        }

        private SqlCommand buildSqlCommand(string query)
        {
            var command = new SqlCommand(query);
            command.Connection = _connection;
            return command;
        }

        private SqlDataReader executeCommandWithReturnValue(SqlCommand command)
        {
            var dataReader = command.ExecuteReader();
            return dataReader;
        }
        private void executeCommandWithoutReturnValue(SqlCommand command)
        {
            var dataReader = command.ExecuteReader();
        }
               
        private DataTable buildDataTableResult(SqlDataReader dataReader)
        {
            DataTable dataTableResult = new DataTable();
            dataTableResult.Load(dataReader);
            return dataTableResult;
        }
        #endregion

        private int GetIntValueFromResult(DataTable dataTableResult)
        {
            DataRow dataRow = dataTableResult.Rows[0];
            var dataRowValue = dataRow[0];

            if (dataRowValue != null & dataRowValue.ToString() != "")
            {
                return Convert.ToInt32(dataRowValue.ToString());
            }
            else
                return 0;
        }
    }
}
