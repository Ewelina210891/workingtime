﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.IO;
using System.Diagnostics;
using System.Globalization;
using System.Drawing;
using WorkingTimeApp.Repositories;
using System.Runtime.InteropServices;
using System.Data;

namespace WorkingTimeApp.ExportData
{
    class ExcelExport
    {
        #region variables
        IRepository _repository;
        Application _excelApplication;
        Workbook _workbook;
        Sheets _worksheets;
        Worksheet _dataWorksheet;
        int _indexOfLastDataInWorksheet;
        #endregion

        #region conctructors
        public ExcelExport(IRepository sqlRepository)
        {
            _repository = sqlRepository;

            _excelApplication = new Application
            {
                SheetsInNewWorkbook = 1,     // ilość arkuszy w pliku
                UserControl = true           // TRUE, jeżeli obiekt Application został stworzony programowo
            };

            _workbook = _excelApplication.Workbooks.Add(Missing.Value);
            _worksheets = _excelApplication.Worksheets;
            _dataWorksheet = (Worksheet)_worksheets.get_Item(1);
        }
        #endregion

        #region generate report
        public void GenerateGeneralReportForOneProject(string filePath, string projectName)
        {
            AddHeaderPartForOneProject("RAPORT OGÓLNY", projectName);
            AddGeneralSummaryPartForOneProject(projectName);
            AddDetailSummaryPartForOneProject(projectName);
            SaveFile(filePath);
        }
        public void GenerateGeneralReportForRange(string filePath, DateTime startDate, DateTime endDate)
        {
            AddHeaderPartForRange("RAPORT OGÓLNY", startDate, endDate);
            AddGeneralSummaryPartForRange(startDate, endDate);
            AddDetailSummaryPartForRange(startDate, endDate);
            SaveFile(filePath);
        }
        public void GenerateDayToDayReporForOneProject(string filePath, string projectName)
        {
            AddHeaderPartForOneProject("RAPORT DZIEŃ PO DNIU", projectName);
            AddGeneralSummaryPartForOneProject(projectName);
            AddDayToDaySummaryPartForOneProject(projectName);
            SaveFile(filePath);
        }
        public void GenerateDayToDayReportForRange(string filePath, DateTime startDate, DateTime endDate)
        {
            AddHeaderPartForRange("RAPORT OGÓLNY", startDate, endDate);
            AddGeneralSummaryPartForRange(startDate, endDate);
            AddDayToDaySummaryPartForRange(startDate, endDate);
            SaveFile(filePath);
        }
        #endregion

        #region header part
        private void AddHeaderPartForOneProject(string reportName, string projectName)
        {
            HeaderTemplate();
            ((Range)_dataWorksheet.Cells[2, 7]).Value2 = reportName;
            ((Range)_dataWorksheet.Cells[3, 7]).Value2 = "dla projektu: " + projectName;

            _dataWorksheet.get_Range("A4", "C4").Value2 = AddNameAndLastname();
            _dataWorksheet.get_Range("I4", "L4").Value2 = AddDateOfGenerateReport();
        }
        private void AddHeaderPartForRange(string reportName, DateTime startDate, DateTime endDate)
        {
            HeaderTemplate();
            ((Range)_dataWorksheet.Cells[2, 7]).Value2 =reportName;
            _dataWorksheet.get_Range("A4", "C4").Value2 = AddNameAndLastname();
            _dataWorksheet.get_Range("I4", "L4").Value2 = AddDateOfGenerateReport();
            _dataWorksheet.get_Range("E6", "I6").Value2 = AddDateRangeOfReport(startDate, endDate);
            ((Range)_dataWorksheet.Cells[6, 5]).Interior.Color = ColorTranslator.ToWin32(Color.Yellow);
            ((Range)_dataWorksheet.Cells[6, 8]).Interior.Color = ColorTranslator.ToWin32(Color.Yellow);
        }

        private void HeaderTemplate()
        {
            _dataWorksheet.get_Range("F2", "H2").Interior.Color = ColorTranslator.ToWin32(Color.Yellow);
            _dataWorksheet.get_Range("F2", "H2").HorizontalAlignment = XlVAlign.xlVAlignCenter;
            _dataWorksheet.get_Range("F3", "H3").HorizontalAlignment = XlVAlign.xlVAlignCenter;

            _dataWorksheet.get_Range("A4", "B4").Interior.Color = ColorTranslator.ToWin32(Color.Yellow);
            _dataWorksheet.get_Range("I4", "K4").Interior.Color = ColorTranslator.ToWin32(Color.Yellow);

        }
        
        private string[,] AddNameAndLastname()
        {
            string[,] data = new string[1, 3];

            data[0,0] = "Imię i nazwisko:";
            data[0,2] = FormWorkingTime.employeeName;

            return data;
        }
        private string[,] AddDateOfGenerateReport()
        {
            string[,] data = new string[1, 4];

            data[0, 0] = "Data wygenerowania raportu:";
            data[0, 3] = DateTime.Now.ToString("HH:mm dd.MM.yyyy ");

            return data;
        }
        private string[,] AddDateRangeOfReport(DateTime startDate, DateTime endDate)
        {
            string[,] data = new string[1, 5];

            data[0,0] = "Zakres:";
            data[0, 1] = startDate.ToShortDateString();
            data[0,3] = "do";
            data[0, 4] = endDate.ToShortDateString();

            return data;
        }
        #endregion

        #region general summary part
        private void AddGeneralSummaryPartForOneProject(string projectName)
        {
            _dataWorksheet.get_Range("A7", "B7").Interior.Color = ColorTranslator.ToWin32(Color.Yellow);
            ((Range)_dataWorksheet.Cells[7, 1]).Value2 = "Zestawienie ogólne:";
            _dataWorksheet.get_Range("B9", "F11").Value2 = AddSummaryOfWorkingHoursForOneProject(projectName);
        }
        private void AddGeneralSummaryPartForRange(DateTime startDate, DateTime endDate)
        {
            _dataWorksheet.get_Range("A8", "B8").Interior.Color = ColorTranslator.ToWin32(Color.Yellow);
            ((Range)_dataWorksheet.Cells[8, 1]).Value2 = "Zestawienie ogólne:";
            _dataWorksheet.get_Range("B10", "F13").Value2 = AddSummaryOfDaysForRange(startDate, endDate);
            _dataWorksheet.get_Range("B15", "F20").Value2 = AddSummaryOfWorkingHoursForRange(startDate, endDate);
        }

        private string[,] AddSummaryOfDaysForRange(DateTime startDate, DateTime endDate)
        {
            string[,] data = new string[4, 5];

            data[0, 0] = "Ilość dni roboczych:";
            data[1, 0] = "Ilość godzin ogółem:";
            //data[2, 0] = "Ilość godzin projektowych:";
           // data[3, 0] = "Ilość godzin administracyjnych:";

            data[0, 4] = _repository.GetDaysOfWorkForRange(startDate, endDate).ToString();
            data[1, 4] = ConvertToTimeString(_repository.GetDaysOfWorkForRange(startDate, endDate) * 8 *60);
           // data[2, 4] = ConvertToTimeString(_repository.GetDaysOfWorkForRange(startDate, endDate) * 8 *48);
           // data[3, 4] = ConvertToTimeString(_repository.GetDaysOfWorkForRange(startDate, endDate) * 8 *12);

            return data;
        }

        private string[,] AddSummaryOfWorkingHoursForOneProject(string projectNmae)
        {
            string[,] data = new string[3, 5];
            data[0, 0] = "Przepracowane godziny ogółem:";
            data[1, 0] = "Przepracowane godziny projektowe:";
            data[2, 0] = "Przepracowane godziny administracyjne:";

            data[0, 4] = ConvertToTimeString(_repository.GetWorkingMinutesForOneProject(projectNmae));
            data[1, 4] = ConvertToTimeString(_repository.GetWorkingProjectMinutesForOneProject(projectNmae));
            data[2, 4] = ConvertToTimeString(_repository.GetWorkingAdministrationMinutesForOneProject(projectNmae));
            return data;
        }
        private string[,] AddSummaryOfWorkingHoursForRange(DateTime startDate, DateTime endDate)
        {
            string[,] data = new string[6,5];
            data[0, 0] = "Przepracowane godziny ogółem:";
            data[1, 0] = "Przepracowane godziny projektowe:";
            data[2, 0] = "Przepracowane godziny administracyjne:";
            data[3, 0] = "Przepracowane godziny weekendowe:";
            data[4, 0] = "Nadgodziny:";
            data[5, 0] = "Urlop:";

            int weekendMinute = _repository.GetWeekendMinutesForRange(startDate, endDate);
            int realMinutesOfWork = _repository.GetWorkingMinutesForRange(startDate, endDate);
            int dayOffMinutes = _repository.GetDayOffHoursForRange(startDate, endDate) * 60;
            int minutesOfWork = (_repository.GetDaysOfWorkForRange(startDate, endDate) * 8 * 60);
            int overtime = realMinutesOfWork - dayOffMinutes - minutesOfWork;

            data[0, 4] = ConvertToTimeString(realMinutesOfWork);
            data[1, 4] = ConvertToTimeString(_repository.GetWorkingProjectMinutesForRange(startDate, endDate));
            data[2, 4] = ConvertToTimeString(_repository.GetWorkingAdministrationMinutesForRange(startDate, endDate));
            data[3, 4] = ConvertToTimeString(weekendMinute);
            data[4, 4] = overtime>0 ? ConvertToTimeString(overtime) : "0";
            data[5, 4] = ConvertToTimeString(dayOffMinutes);

            return data;
        }
        #endregion

        #region detail summary part
        private void AddDetailSummaryPartForOneProject(string projectName)
        {
            _indexOfLastDataInWorksheet = 14;

            ((Range)_dataWorksheet.Cells[14, 1]).Value2 = "Zestawienie szczegółowe:";
            _dataWorksheet.get_Range("A14", "C14").Interior.Color = Color.Yellow;

            AddDetailSummaryForOneProject(projectName);
        }
        private void AddDetailSummaryPartForRange(DateTime startDate, DateTime endDate)
        {

            _indexOfLastDataInWorksheet = 22;

            ((Range)_dataWorksheet.Cells[22, 1]).Value2 = "Zestawienie szczegółowe:";
            _dataWorksheet.get_Range("A22", "C22").Interior.Color = Color.Yellow;

            AddDetailSummaryForRange(startDate, endDate);
        }

        private void AddDetailSummaryForOneProject(string projectName)
        {
            _indexOfLastDataInWorksheet += 2;

            var data = AddProjectToDetailSummaryForOneProject(projectName);

            var index1 = "B" + _indexOfLastDataInWorksheet;

            var index2 = "L" + (_indexOfLastDataInWorksheet + data.GetLength(0) - 1);
                _dataWorksheet.get_Range(index1, index2).Value2 = data;
                
                _dataWorksheet.get_Range("B" + _indexOfLastDataInWorksheet, "L" + _indexOfLastDataInWorksheet).Interior.Color = Color.Wheat;
                _dataWorksheet.get_Range("K" + (_indexOfLastDataInWorksheet + data.GetLength(0) - 1), "L" + (_indexOfLastDataInWorksheet + data.GetLength(0) - 1)).Interior.Color = Color.LightCoral;

                _indexOfLastDataInWorksheet += data.GetLength(0) -1;

        }
        private void AddDetailSummaryForRange(DateTime startDate, DateTime endDate)
        {

            var projects = _repository.GetProjectsForRange(startDate, endDate);
            foreach (DataRow dtRow in projects.Rows)
            {
                _indexOfLastDataInWorksheet += 2;
                var projectName = dtRow[0].ToString();
                var data = AddOneProjectToDetailSummaryForRange(projectName, startDate, endDate);

                var index1 = "B" + _indexOfLastDataInWorksheet;

                var index2 = "L" + (_indexOfLastDataInWorksheet + data.GetLength(0) - 1);
                _dataWorksheet.get_Range(index1, index2).Value2 = data;

                _dataWorksheet.get_Range("B" + _indexOfLastDataInWorksheet, "L" + _indexOfLastDataInWorksheet).Interior.Color = Color.Wheat;
                _dataWorksheet.get_Range("K" + (_indexOfLastDataInWorksheet + data.GetLength(0) - 1), "L" + (_indexOfLastDataInWorksheet + data.GetLength(0) - 1)).Interior.Color = Color.LightCoral;

                _indexOfLastDataInWorksheet += data.GetLength(0) - 1;
            }

        }
        
        private string[,] AddProjectToDetailSummaryForOneProject(string projectName)
        {
            var jobs = _repository.GetAllJobsAndTimeForProject(projectName);

            string[,] data = new string[jobs.Rows.Count + 2, 11];

            data[0, 0] = "Wykonane zadania";
            data[0, 10] = "Czas";

            int jobTimeMinutes;
            int index = 1;
            int sumOfJobTime= 0;
            
            foreach (DataRow dtRow in jobs.Rows)
            {
                var jobDescription = dtRow[0].ToString();

                jobTimeMinutes = Convert.ToInt32(dtRow[1].ToString());

                data[index,0] = jobDescription;
                data[index, 10] = ConvertToTimeString(jobTimeMinutes);

                index++;

                sumOfJobTime = sumOfJobTime + jobTimeMinutes;
            }
            data[index, 9] = "Suma";
            data[index, 10] = ConvertToTimeString(sumOfJobTime);

            return data;
        }
        private string[,] AddOneProjectToDetailSummaryForRange(string projectName, DateTime startDate, DateTime endDate)
        {
            var jobs = _repository.GetJobsAndTimeForProjectForRange(projectName, startDate, endDate);

            string[,] data = new string[jobs.Rows.Count + 2, 11];

            data[0, 0] = "Nazwa projektu";
            data[0, 2] = "Wykonane zadania";
            data[0, 10] = "Czas";

            data[1,0] = projectName;

            int index = 1;
            int sumOfJobTime = 0;
            int jobTimeMinutes=0;

            foreach (DataRow dtRow in jobs.Rows)
            {
                var jobDescription = dtRow[0].ToString();

                DateTime jobTime = new DateTime();
                jobTimeMinutes = Convert.ToInt32(dtRow[1].ToString());
                jobTime = jobTime.AddMinutes(jobTimeMinutes);

                data[index,2] = jobDescription;
                data[index, 10] = ConvertToTimeString(jobTimeMinutes);
                index++;

               sumOfJobTime += jobTimeMinutes;
            }
            data[index, 9] = "Suma";
            data[index, 10] = ConvertToTimeString(sumOfJobTime);

            return data;
        }
        #endregion

        #region day to day summary part
        private void AddDayToDaySummaryPartForOneProject(string projectName)
        {
            ((Range)_dataWorksheet.Cells[13, 1]).Value2 = "Zestawienie dzień po dniu:";
            _dataWorksheet.get_Range("A13", "C13").Interior.Color = Color.Yellow;

            var dates = _repository.GetWorkingDaysForOneProject(projectName);

            _indexOfLastDataInWorksheet = 14;


            _dataWorksheet.get_Range("B15","P15").Value2 = AddHeadersOfDayToDayTableForOneProject();
            _dataWorksheet.get_Range("B15", "P15").Interior.Color = Color.LightCoral;

            foreach (DataRow dtRow in dates.Rows)
            {
                _indexOfLastDataInWorksheet += 2;
                var date = (DateTime)dtRow[0];

                var data = AddOneDayInDayToDaySummaryForOneProject(date, projectName);

                var index1 = "B" + _indexOfLastDataInWorksheet;

                var index2 = "R" + (_indexOfLastDataInWorksheet + data.GetLength(0) - 1);
                _dataWorksheet.get_Range(index1, index2).Value2 = data;

                _indexOfLastDataInWorksheet += data.GetLength(0) - 1;
            }
        }
        private void AddDayToDaySummaryPartForRange(DateTime startDate, DateTime endDate)
        {
            ((Range)_dataWorksheet.Cells[21, 1]).Value2 = "Zestawienie dzień po dniu:";
            _dataWorksheet.get_Range("A21", "C21").Interior.Color = Color.Yellow;
            _indexOfLastDataInWorksheet = 22;
            _dataWorksheet.get_Range("B23", "P23").Value2 = AddHeadersOfDayToDayTableForRange();

            for (DateTime date = startDate; date < endDate; date= date.AddDays(1.0))
            {                            
                    var data = AddOneDayInDayToDaySummaryForRange(date);

                    if (data.GetLength(0) != 0)
                    {
                        _indexOfLastDataInWorksheet += 2;
                        var index1 = "B" + _indexOfLastDataInWorksheet;

                        var index2 = "R" + (_indexOfLastDataInWorksheet + data.GetLength(0) - 1);
                        _dataWorksheet.get_Range(index1, index2).Value2 = data;

                        _indexOfLastDataInWorksheet += data.GetLength(0) - 1;
                    }
            }
        }
        private string[,] AddHeadersOfDayToDayTableForOneProject()
        {
            string[,] data = new string[1, 17];
            _dataWorksheet.get_Range("B15", "R15").Interior.Color = Color.LightCoral;

            data[0, 0] = "Dzień";
            data[0, 2] = "Wykonane zadania";
            data[0, 7] = "Rozpoczęto";
            data[0, 9] = "Zakończono";
            data[0, 11] = "Delegacja";
            data[0, 12] = "Komentarz";

            return data;
        }
        private string[,] AddHeadersOfDayToDayTableForRange()
        {
            string[,] data = new string[1,17];
            _dataWorksheet.get_Range("B23", "R23").Interior.Color = Color.LightCoral;

            data[0, 0] = "Dzień";
            data[0, 2] = "Nr projektu";
            data[0, 4] = "Wykonane zadania";
            data[0, 9] = "Rozpoczęto";
            data[0, 11] = "Zakończono";
            data[0, 13] = "Delegacja";
            data[0, 14] = "Komentarz";

            return data;
        }

        private string[,] AddOneDayInDayToDaySummaryForOneProject(DateTime date, string projectNumber)
        {
            var jobs = _repository.GetPerformedJobsForOneProjectOnOneDay(date, projectNumber);

            string[,] data = new string[jobs.Rows.Count, 17];

            data[0, 0] = date.ToString("dd.MM.yyyy");

            int index = 0;

            foreach (DataRow dtRow in jobs.Rows)
            {
                data[index, 2] = dtRow[3].ToString();
                data[index, 7] = dtRow[4].ToString();
                data[index, 9] = dtRow[5].ToString();
                data[index, 11] = (bool)dtRow[6] ? "tak" : "nie";
                data[index, 12] = dtRow[7].ToString();

                index++;
            }

            return data;


        }
        private string[,] AddOneDayInDayToDaySummaryForRange(DateTime date)
        {
            var jobs = _repository.GetPerformedJobsForAllProjectsOnOneDay(date);

            string[,] data = new string[jobs.Rows.Count, 17];

            if (jobs.Rows.Count != 0)
            {

                data[0, 0] = date.ToString("dd.MM.yyyy");

                int index = 0;

                foreach (DataRow dtRow in jobs.Rows)
                {
                    data[index, 2] = dtRow[2].ToString();
                    data[index, 4] = dtRow[3].ToString();
                    data[index, 9] = dtRow[4].ToString();
                    data[index, 11] = dtRow[5].ToString();
                    data[index, 13] = (bool)dtRow[6] ? "tak" : "nie";
                    data[index, 14] = dtRow[7].ToString();

                    index++;
                }
            }
            return data;


        }
        #endregion

        private void SaveFile(string filePath)
        {
            if (File.Exists(filePath))
                File.Delete(filePath);

            _workbook.Close(true, filePath, Missing.Value);
            _excelApplication.Quit();
            KillProcess();
        }

        private void KillProcess()
        {
            CultureInfo oldCI = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var proc = Process.GetProcesses();
            for (var i = 0; i < proc.Length; ++i)
                if (proc[i].ProcessName.StartsWith("EXCEL") && proc[i].MainWindowTitle == "")
                    proc[i].Kill();

            System.Threading.Thread.CurrentThread.CurrentCulture = oldCI;

        }

        private string ConvertToTimeString(int minutes)
        {
            var hour = minutes / 60;
            var minute = minutes % 60;

            if 
                (minute < 10) return hour + ":0" + minute;
            else
                return hour + ":" + minute;
        }
    }
}
