#Opis programu
Aplikacja służąca do dokumentacji zadaniowego czasu pracy z możliwością generowania raportów w formacie .xlsx (z danego zakresu datowego lub dla wybranego projektu). Została ona stworzona na indywidualne zamówienie i dostosowana do potrzeb klienta.


##Wersje

Dostępne są dwie wersje programu, umieszczone w osobnych folderach:

* dla bazy SQLite- plik instalacyjny znajduje się w folderze

* dla bazy MsSQL Server 2012